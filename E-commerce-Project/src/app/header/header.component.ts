import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataService } from '../services/data.service';
import { Globals } from '../services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { Output, EventEmitter } from '@angular/core'; 

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Output() someEvent = new EventEmitter<string>();
  searchShow: boolean;
  shownow:boolean;
  constructor(
    public cookieService: CookieService,
    private router: Router,
    private spinner: NgxSpinnerService,
    public dataService: DataService,
    public globals: Globals,
    private toastr: ToastrService
  ) {}
  ngOnInit(): void {
    this.globals.id = this.cookieService.get('id');
  }
  productlist() {
    console.log(this.globals.search);
    this.router.navigate(['/', 'product']);
    this.someEvent.next();
  }
  logout() {
    this.cookieService.deleteAll();
    window.location.reload();
  }
  seachClick(){
    this.searchShow = !this.searchShow;   
  }
  shownowon(){
    this.shownow = !this.shownow;
  }
}
