import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataService } from 'src/app/services/data.service';
import { Globals } from 'src/app/services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { Urlcall } from '../services/urll';
import { product } from '../models/product';

@Component({
  selector: 'app-subcategory-products',
  templateUrl: './subcategory-products.component.html',
  styleUrls: ['./subcategory-products.component.scss']
})
export class SubcategoryProductsComponent implements OnInit {
  data: any;
  product = new product;

  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }



  ngOnInit(): void {
    
    this.globals.subCatId = this.cookieService.get('subCatId');
    this.globals.CatId = this.cookieService.get('CatId');
    if(this.globals.subCatId == "" ){
      this. Productcategory();
    }else{
     
      this.getdata();
    }
    
    
   
  }
  getdata(){
    this.spinner.show();
    this.dataService.get(Urlcall.fetchUserSubcategory+this.globals.subCatId).subscribe(res=>{
      this.spinner.hide();
      this.data = res['data'];
    },err=>{ this.spinner.hide();})
  }
  productview(id){
    this.globals.product_id =id; 
    this.cookieService.set('product_id',this.globals.product_id);
    console.log('productid',id);
    this.router.navigate(['/Product-View'] ,{ queryParams: { id : id}});   
  }


  addcart(id){ 
    if(this.globals.id){
    let data={
      user_id: this.globals.id,
      product_id: id
    }
   this.spinner.show();
    this.dataService.post(Urlcall.cartAdd,data).subscribe(res=>{
      this.spinner.hide();
       this.data = res['data'];
       this.router.navigate(['/','Cart'])
  },err=>{ this.spinner.hide();})
  }else{
    this.router.navigate(['/signin'])
  }
  }
  
  
  Productcategory(){
    this.spinner.show();
    this.dataService.get(Urlcall.fetchProductByCategory+this.globals.CatId).subscribe(res=>{
      this.spinner.hide();
      this.data= res['data'];
    },err=>{ this.spinner.hide();})
  }



}
