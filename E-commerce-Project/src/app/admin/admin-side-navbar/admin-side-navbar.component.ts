import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataService } from 'src/app/services/data.service';
import { Globals } from 'src/app/services/globalvariable';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-admin-side-navbar',
  templateUrl: './admin-side-navbar.component.html',
  styleUrls: ['./admin-side-navbar.component.scss'],
})
export class AdminSideNavbarComponent implements OnInit {
  $collaps_One: boolean;
  sidenav:boolean;
  constructor(
    private modalService: NgbModal,
    public cookieService: CookieService,
    private router: Router,
    private spinner: NgxSpinnerService,
    public dataService: DataService,
    public globals: Globals,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.globals.id = this.cookieService.get('id');
    this.globals.role = this.cookieService.get('role');
    if (this.globals.role != 'admin') {
      this.router.navigate(['/', '/']);
    }
  }
  logout() {
    this.cookieService.deleteAll();
    window.location.reload();
  }

  onControlCollapse1() {
    this.$collaps_One = true;
  }
  sidenavClick(){
    this.globals.sidenav = !this.globals.sidenav; 
  }
}
