import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataService } from '../services/data.service';
import { Globals } from '../services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { Urlcall } from '../services/urll';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.scss']
})
export class ForgotpasswordComponent implements OnInit {
  mobileno: any;
  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }


  ngOnInit(): void {



  }


  generateOtp(){ 
    
    let data={
      
      
      mobile: this.mobileno
      
    
      }
    
   this.spinner.show();
    this.dataService.post(Urlcall.GenerateOtp,data).subscribe(res=>{
      this.spinner.hide();
      this.toastr.success(res['message'])
       let temp = res['data'];
       console.log('temp')
       if (res['status']){
         this.globals.user_id = temp.userId;
         this.cookieService.set('user_id',this.globals.user_id);
         this.router.navigate(['/','verifyotp'])
       }
      },err=>{ this.spinner.hide();
        this.toastr.success(err['message'])})
     }
}


