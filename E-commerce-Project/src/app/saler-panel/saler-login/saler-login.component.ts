import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from 'src/app/services/data.service'; 
import { Globals } from 'src/app/services/globalvariable'; 
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Urlcall } from 'src/app/services/urll';
import { selrguser } from 'src/app/models/selr-reg';


@Component({
  selector: 'app-saler-login',
  templateUrl: './saler-login.component.html',
  styleUrls: ['./saler-login.component.scss']
})
export class SalerLoginComponent implements OnInit {

  user = new selrguser;
  
  constructor(public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }



  ngOnInit(): void {
  }
  next(url){
    this.router.navigate(['/',url])
  }

  runMyAPI(){ 
    this.spinner.show();
     this.dataService.post(Urlcall.sellerReg,this.user).subscribe(res=>{
       this.spinner.hide();
       this.toastr.success(res['message'])
       if(res['status']){
         console.log(res);
         this.globals.id = res['data']._id;
         this.globals.role = res['data'].role;
         this.cookieService.set('id',this.globals.id);
         this.cookieService.set('role',this.globals.role);
         
         
          this.next('SalerHome');
         }
        
       
     },err=>{ this.spinner.hide();
      this.toastr.success(err['message'])})
   }
  seller(Reg: any, seller: any) {
    throw new Error('Method not implemented.');
    
  }
}




