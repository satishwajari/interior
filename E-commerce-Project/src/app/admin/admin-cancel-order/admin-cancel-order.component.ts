import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from '../../services/data.service';
import { Globals } from '../../services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Urlcall } from '../../services/urll';
import { address } from 'src/app/models/address';
import { product } from 'src/app/models/product';
import { selrguser } from 'src/app/models/selr-reg';

@Component({
  selector: 'app-admin-cancel-order',
  templateUrl: './admin-cancel-order.component.html',
  styleUrls: ['./admin-cancel-order.component.scss']
})
export class AdminCancelOrderComponent implements OnInit {
  data: any;
  currentAddr = new address;
  product = new product;
  currentImage: any;
  sellerData = new selrguser;
  selrguser = new selrguser;

  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }


  ngOnInit(): void {

    this.globals.id = this.cookieService.get('id');
   this.getData();
  }
  getData(){
    this.spinner.show();
    this.dataService.get(Urlcall.fetchCancelOrderAdmin).subscribe(res=>{
      this.spinner.hide();
      this.data = res['data'];
    },err=>{this.spinner.hide();})
    }

    address(addressItem){
      this.currentAddr = addressItem;
      console.log("Curent Addr : " + JSON.stringify(this.currentAddr));
    }

    edit(rel){
      this.product =rel;
      this.currentImage = this.product.image[0].img_name;
      console.log(this.product);
    }
    selller(sellerData){
      this.selrguser =sellerData;
      console.log("seller Data : " + JSON.stringify(this.sellerData));
    }
}
