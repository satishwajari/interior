import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from '../services/data.service'; 
import { Globals } from '../services/globalvariable'; 
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute  } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Urlcall } from '../services/urll';
import { consultant } from '../models/consultant';
import { interiorproduct } from '../models/interior-product';

@Component({
  selector: 'app-book-consultant',
  templateUrl: './book-consultant.component.html',
  styleUrls: ['./book-consultant.component.scss']
})
export class BookConsultantComponent implements OnInit {
  consultantdetails = new consultant;
  interiorproduct = new interiorproduct;
  data = new interiorproduct;
  currentImage: any;
  categoryData: any;
  productNameData: any;
  subCategoryData: any;

  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService,private activatedRoute: ActivatedRoute, public globals: Globals, private toastr: ToastrService) {  
      this.activatedRoute.queryParams.subscribe(params => {
        let id = params['id'];
       this.globals.product_id = id;
       this.getdata();
       
    });

    }

  ngOnInit(): void {
   
  }
  bookconsultant(){
    this.spinner.show();
    this.dataService.post(Urlcall.addConsultant,this.consultantdetails).subscribe(res=>{
      this.spinner.hide();
      this.toastr.success(res['message'])
       
      
  },err=>{ this.spinner.hide();
    this.toastr.success(err['message'])})
 }
 getdata(){
  this.spinner.show();
  this.dataService.get(Urlcall.fetchproductData+this.globals.product_id).subscribe(res=>{
    this.spinner.hide();
    this.consultantdetails.product_id = this.globals.product_id;
    const temp=res['data'];
    this.data = temp[0];
    this.productNameData = this.data['productName']
    this.categoryData = this.data['category']
    this.subCategoryData = this.data['subCategory']
    this.currentImage = this.data.image[0].img_name;

  },err=>{ this.spinner.hide();})
}
}
