import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalerReturnComponent } from './saler-return.component';

describe('SalerReturnComponent', () => {
  let component: SalerReturnComponent;
  let fixture: ComponentFixture<SalerReturnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalerReturnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalerReturnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
