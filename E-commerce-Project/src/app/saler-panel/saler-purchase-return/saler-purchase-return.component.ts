import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from '../../services/data.service';
import { Globals } from '../../services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Urlcall } from '../../services/urll';
import { address } from 'src/app/models/address';
import { product } from 'src/app/models/product';

@Component({
  selector: 'app-saler-purchase-return',
  templateUrl: './saler-purchase-return.component.html',
  styleUrls: ['./saler-purchase-return.component.scss']
})
export class SalerPurchaseReturnComponent implements OnInit {
  data: any;
  currentAddr = new address;
  product = new product;
  currentImage: any;

  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.globals.id = this.cookieService.get('id');
    this. getdata();


  }
  getdata(){
    this.spinner.show();
    this.dataService.get(Urlcall.fetchCancelOrderSeller+this.globals.id).subscribe(res=>{
      this.spinner.hide();
      this.data = res['data'];
    },err=>{ this.spinner.hide();})
  }
  address(addressItem){
    this.currentAddr = addressItem;
    console.log("Curent Addr : " + JSON.stringify(this.currentAddr));
  }

  edit(rel){
    this.product =rel;
    this.currentImage = this.product.image[0].img_name;
    console.log(this.product);
  }



}
