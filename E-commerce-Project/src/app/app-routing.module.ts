import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MainareaComponent } from './mainarea/mainarea.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { AboutComponent } from './about/about.component';
import { CartComponent } from './cart/cart.component';
import { SalereHomeComponent } from './saler-panel/salere-home/salere-home.component';
import { SalerLoginComponent } from './saler-panel/saler-login/saler-login.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { AllCatgoriesComponent } from './all-catgories/all-catgories.component';
import {UserRegistrationComponent} from './user-registration/user-registration.component';
import { LoginComponent} from './login/login.component';
import {OtpComponent} from './otp/otp.component'
import { AdminhomeComponent } from './admin/adminhome/adminhome.component';
import { AdminproductComponent } from './admin/adminproduct/adminproduct.component';
import { AdminCatgoriesComponent } from './admin/admin-catgories/admin-catgories.component';
import { AdminSellerlistComponent } from './admin/admin-sellerlist/admin-sellerlist.component';
import { AdminSubcatgoryComponent } from './admin/admin-subcatgory/admin-subcatgory.component';
import { AdminProductListComponent } from './admin/admin-product-list/admin-product-list.component';
import { SellerRegComponent } from './seller-reg/seller-reg.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { TermsConditinComponent } from './terms-conditin/terms-conditin.component';
import { AdminVendorListComponent } from './admin/admin-vendor-list/admin-vendor-list.component';
import { AdminSaleComponent } from './admin/admin-sale/admin-sale.component';
import { AdminCancelOrderComponent } from './admin/admin-cancel-order/admin-cancel-order.component';
import { AdminReturnOrderComponent } from './admin/admin-return-order/admin-return-order.component';
import { SalerOrderCalcelComponent } from './saler-panel/saler-order-calcel/saler-order-calcel.component';
import { SalerPurchaseComponent } from './saler-purchase/saler-purchase.component';
import { SalerPurchaseReturnComponent } from './saler-panel/saler-purchase-return/saler-purchase-return.component';
import { SalerReturnComponent } from './saler-panel/saler-return/saler-return.component';
import { SalerSaleComponent } from './saler-panel/saler-sale/saler-sale.component';
import { AdminBannerComponent } from './admin/admin-banner/admin-banner.component';
import { SalerProductAddComponent } from './saler-panel/saler-product-add/saler-product-add.component';
import { SellerProductListComponent } from './seller-product-list/seller-product-list.component';
import { ProductListComponent } from './product-list/product-list.component';
import { SaperProfileComponent } from './saler-panel/saper-profile/saper-profile.component';
 import { EditUserProfileComponent } from './edit-user-profile/edit-user-profile.component';
 import { UserProfileComponent } from './user-profile/user-profile.component';
 import { ChangePasswordComponent } from './change-password/change-password.component';
 import { SubcategoryComponent } from './subcategory/subcategory.component';
 import { SubcategoryProductsComponent } from './subcategory-products/subcategory-products.component';

import { AddressComponent } from './address/address.component';
import { PaymentComponent } from './user-panel/payment/payment.component';
import { ThankYouComponent } from './user-panel/thank-you/thank-you.component';
import { OrderHistoryComponent } from './user-panel/order-history/order-history.component';
import { RecentOrderComponent } from './saler-panel/recent-order/recent-order.component';
import { TransactionComponent } from './transaction/transaction.component';
import { OtpChangePasswordComponent } from './otp-change-password/otp-change-password.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { VerifyOtpComponent } from './verify-otp/verify-otp.component';
import { AdminSellerProductlistComponent } from './admin/admin-seller-productlist/admin-seller-productlist.component';
import { AdminSubcategoryProductlistComponent } from './admin/admin-subcategory-productlist/admin-subcategory-productlist.component';
import { AdminAllordersComponent } from './admin/admin-allorders/admin-allorders.component';
import { UserEditAddressComponent } from './user-panel/user-edit-address/user-edit-address.component';
import { AdminUserListComponent } from './admin/admin-user-list/admin-user-list.component';
import { InteriorDesignComponent } from './interior-design/interior-design.component';
import { AdminUserorderListComponent } from './admin/admin-userorder-list/admin-userorder-list.component';
import { getParsedCommandLineOfConfigFile } from 'typescript';
import { BookConsultantComponent } from './book-consultant/book-consultant.component';

import { InteriorProductComponent } from './admin/interior-product/interior-product.component';
import { AdminInteriorCategoriesComponent } from './admin/admin-interior-categories/admin-interior-categories.component';
import { AdminInteriorSubcategoriesComponent } from './admin/admin-interior-subcategories/admin-interior-subcategories.component';
import { InteriorSubproductsComponent } from './interior-subproducts/interior-subproducts.component';


const routes: Routes = [
  
  { path: '', component: MainareaComponent },
  { path: 'Address', component: AddressComponent },
  { path: 'edituseradress', component: UserEditAddressComponent },
  { path: 'Profile', component: UserProfileComponent },
  { path: 'password', component: ChangePasswordComponent },
  { path: 'subproduct', component: SubcategoryProductsComponent },
  { path: 'subcategory', component: SubcategoryComponent },
  { path: 'transaction',component: TransactionComponent},
  { path: 'otppassword',component: OtpChangePasswordComponent},
  { path: 'forgotpassword',component: ForgotpasswordComponent},
  { path: 'verifyotp',component: VerifyOtpComponent},
  { path:'adminseller',component: AdminSellerProductlistComponent},

  { path: 'Edit_Profile', component: EditUserProfileComponent },
  { path: 'AdminBanner', component: AdminBannerComponent },
  { path: 'otp', component: OtpComponent },
  { path: 'Home', component: MainareaComponent },
  { path: 'About', component: AboutComponent },
  { path: 'Product-View', component: ProductDetailsComponent },
  { path: 'Cart', component: CartComponent },
  { path: 'SalerHome', component: SalereHomeComponent },
  { path: 'SaleOrderCancel', component: SalerOrderCalcelComponent },
  { path: 'Sale', component: SalerSaleComponent },
  { path: 'SalePurchase', component: SalerPurchaseComponent },
  { path: 'SaleReturn', component: SalerPurchaseReturnComponent },
  { path: 'SalerRegisater', component: SalerLoginComponent },
  { path: 'Contact', component: ContactUsComponent },
  { path: 'all-category', component: AllCatgoriesComponent },
  { path: 'registration', component: UserRegistrationComponent },
  { path: 'signin', component: LoginComponent },
  { path: 'Admin', component: AdminhomeComponent },
  { path: 'Admin-Products', component: AdminproductComponent },
  { path: 'Admin-Category', component: AdminCatgoriesComponent },
  { path: 'Admin-salelist', component: AdminSellerlistComponent },
  { path: 'Admin-Subcategory', component: AdminSubcatgoryComponent },
  { path: 'Admin-Product-List', component: AdminProductListComponent },
  { path: 'Vendor-List', component: AdminVendorListComponent },
  { path: 'User-List',component: AdminUserListComponent},
  { path: 'Admin-Sale', component: AdminSaleComponent },
  { path: 'Admin-Cancel-Order', component: AdminCancelOrderComponent },
  { path: 'AdminReturn-Order', component: AdminReturnOrderComponent },
  { path: 'banner', component: AdminBannerComponent },
  { path: 'seller-reg', component: SellerRegComponent },
  { path: 'privacy', component: PrivacyPolicyComponent },
  { path: 'terms', component: TermsConditinComponent },
  { path: 'SalerProduct', component: SalerProductAddComponent },
  { path: 'seller-product-list', component: SellerProductListComponent },
  { path: 'product', component: ProductListComponent },
  { path: 'SalerProfile', component: SaperProfileComponent },
  { path: 'User-Payment', component: PaymentComponent },
  { path: 'Payment-Message', component: ThankYouComponent },
  { path: 'Order-History', component: OrderHistoryComponent },
  { path: 'Recent-order', component: RecentOrderComponent },
  { path:'subcategoryproductlist', component: AdminSubcategoryProductlistComponent},
  { path:'allorder', component: AdminAllordersComponent},
  { path:'interior-design', component: InteriorDesignComponent },
  { path: 'userorder-list', component: AdminUserorderListComponent},
  { path: 'book-consultant', component: BookConsultantComponent},
  { path: 'interior-product', component: InteriorProductComponent},
  { path: 'admin-interior-category', component: AdminInteriorCategoriesComponent},
  { path: 'admin-interior-subcategory', component: AdminInteriorSubcategoriesComponent},
  { path: 'interior-subproducts', component: InteriorSubproductsComponent},
 
 
];
 
;
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
