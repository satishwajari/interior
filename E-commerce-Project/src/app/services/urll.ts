export class Urlcall {
    static reSendOTP(reSendOTP: any, data: { user_id: any; }) {
      throw new Error('Method not implemented.');
    }

    // static baseUrl = "http://18.188.173.93:6500";

    static baseUrl = "http://18.188.173.93:6500";

    static userReg = `${Urlcall.baseUrl}/api/user/userRegistration`;
    static userLogin = `${Urlcall.baseUrl}/api/user/login`;
    static sellerReg = `${Urlcall.baseUrl}/api/user/sellerRegister`;

    static cateogaryAdd = `${Urlcall.baseUrl}/api/category/add`;
    static fetchAllCateogary = `${Urlcall.baseUrl}/api/category/fetch`;
    static subCatAdd = `${Urlcall.baseUrl}/api/subcategory/add`;
    static fetchAllSubCat = `${Urlcall.baseUrl}/api/subcategory/fetch?category_id=`;
    static fetchSeller = `${Urlcall.baseUrl}/api/user/fetch?role=seller`;
    
    static fetchdashboardData = `${Urlcall.baseUrl}/api/dashboard/customerdashboard`;
    static fetchproductData = `${Urlcall.baseUrl}/api/product/fetchbyid?_id=`;
    static fetchbannerData = `${Urlcall.baseUrl}/api/banner/fetch`;
    static fetchAllProduct = `${Urlcall.baseUrl}/api/product/fetch`;

    static Addproduct = `${Urlcall.baseUrl}/api/product/add`;
    static deletesubcategory = `${Urlcall.baseUrl}/api/subcategory/delete?_id=`;
    static deletecategory = `${Urlcall.baseUrl}/api/category/delete?_id=`;
    static deletebanner = `${Urlcall.baseUrl}/api/banner/delete?_id=`;
    static addbanner = `${Urlcall.baseUrl}/api/banner/add`;
    static fetchproductstatus = `${Urlcall.baseUrl}/api/product/fetchbystatusandsellerid`;
    static deleteproduct = `${Urlcall.baseUrl}/api/product/delete?_id=`;
    static fetchproduct = `${Urlcall.baseUrl}/api/product/fetchbystatus`;
    static userupdate = `${Urlcall.baseUrl}/api/user/updatedetaile`;
    static productupdate = `${Urlcall.baseUrl}/api/product/updatestatus`;
    static cartAdd = `${Urlcall.baseUrl}/api/cart/add`;
    static fetchproductName = `${Urlcall.baseUrl}/api/product/fetchbyname?productName=`;
    static fetchcart = `${Urlcall.baseUrl}/api/cart/fetch?user_id=`;
    static placeorder = `${Urlcall.baseUrl}/api/order/placeorder`
    static deletecart = `${Urlcall.baseUrl}/api/cart/delete?cart_id=`;
    static transactionAdd = `${Urlcall.baseUrl}/api/transaction/add`;
    static fetchUserById = `${Urlcall.baseUrl}/api/user/findbyid?_id=`;
    static changepassword= `${Urlcall.baseUrl}/api/user/changepassword`;
    static fetchSubcategoryStatus = `${Urlcall.baseUrl}/api/product/fetchbysubcategoryandstatus?subCategory_id=`
   
    static sellerDashboad = `${Urlcall.baseUrl}/api/transaction/sellerDashboard?seller_id=`;
    
    static FetchdeliveryAddress = `${Urlcall.baseUrl}/api/deliveryAddress/fetch?user_id=`;
    static AddDeliveryAddress = `${Urlcall.baseUrl}/api/deliveryAddress/add`;
    static updateProduct = `${Urlcall.baseUrl}/api/product/update`;
    static updateIMAGE =`${Urlcall.baseUrl}/api/product/updateimage`;
    static deleteImage =`${Urlcall.baseUrl}/api/product/deleteimage?image_id=`;
    static orderHistory = `${Urlcall.baseUrl}/api/order/fetch?user_id=`;
    static productCost = `${Urlcall.baseUrl}/api/deliveryAddress/charge?user_id=`;
    static updateDetail = `${Urlcall.baseUrl}/api/user/updatedetaile`;
    static fetchRecentOrder = `${Urlcall.baseUrl}/api/transaction/recentOrders?seller_id=`;
    static updateOrder = `${Urlcall.baseUrl}/api/transaction/updateOrderStatus`;
    static updateQuantity =  `${Urlcall.baseUrl}/api/cart/quantity`;
    static fetchTransportCharges = `${Urlcall.baseUrl}/api/deliveryAddress/transportCharges?user_id=`;
    static fetchRecentOrderTOPTen =`${Urlcall.baseUrl}/api/transaction/recentOrderstopTen?seller_id=`;
    static fetchRecentOrderAdmin = `${Urlcall.baseUrl}/api/transaction/recentOrdersforAdmin`;
    static updateCancelOrder = `${Urlcall.baseUrl}/api/transaction/cancelOrder`;
    static fetchCancelOrderAdmin = `${Urlcall.baseUrl}/api/transaction/cancelOrdersforAdmin`;
    static fetchAdminSale     =      `${Urlcall.baseUrl}/api/transaction/adminpanelSale`;
    static fetchCancelOrderSeller = `${Urlcall.baseUrl}/api/transaction/cancelOrders?seller_id=`;
    static transportCharge = `${Urlcall.baseUrl}/api/transport/transportCharges`;
    static GenerateOtp = `${Urlcall.baseUrl}/api/otp/generateOTP`;
    static OtpVerification = `${Urlcall.baseUrl}/api/otp/otpVerification`;
    static OtpChangePassword = `${Urlcall.baseUrl}/api/otp/changePassword`;
    static AdminSellerUpdate = `${Urlcall.baseUrl}/api/user/updatesellerstatusbyadmin`;
    static updateAddress = `${Urlcall.baseUrl}/api/deliveryAddress/update`;
    static deleteAddress = `${Urlcall.baseUrl}/api/deliveryAddress/delete?_id=`;
    static fetchAllOrderAdmin = `${Urlcall.baseUrl}/api/transaction/allordersforAdmin?orderStatus=`;
    static fetchSellerAllOrder = `${Urlcall.baseUrl}/api/transaction/findallordersforSeller?seller_id=`;
    static updateCategory = `${Urlcall.baseUrl}/api/category/update`;
    static updateSubcategory = `${Urlcall.baseUrl}/api/subcategory/update`;
    static fetchTransaction = `${Urlcall.baseUrl}/api/transaction/transactionList`;
    static updatePrice = `${Urlcall.baseUrl}/api/product/updatestatus`;
    static fetchUserSubcategory = `${Urlcall.baseUrl}/api/product/fetchbysubcategoryforUser?subCategory_id=`;
    static transCharge = `${Urlcall.baseUrl}/api/transport/charge`;
    static adminDash = `${Urlcall.baseUrl}/api/transaction/adminDashboard`;
    static fetchTracking = `${Urlcall.baseUrl}/api/order/tracking?order_id=`;
    static updateUserAdd = `${Urlcall.baseUrl}/api/user/editaddress`;
    static fetchProductByCategory = `${Urlcall.baseUrl}/api/product/productfetchbyCategory?category_id=`
    static reSendOtp = `${Urlcall.baseUrl}/api/user/resendOTP`;
    static fetchBankDetail = `${Urlcall.baseUrl}/api/bankds/fetch?user_id=`;
    static updateBankDetail = `${Urlcall.baseUrl}/api/bankds/add`;
    static addConsultant = `${Urlcall.baseUrl}/api/user/addConsultant`;
    static fetchConsultant = `${Urlcall.baseUrl}/api/user/fetchAllConsultant`;
    static fetchUser = `${Urlcall.baseUrl}/api/user/fetch?role=user`;
    static userdelete = `${Urlcall.baseUrl}/api/user/delete?_id=`;
    static interiorProduct = `${Urlcall.baseUrl}/api/product/interiorProductAdd`;
    static interiorCategory = `${Urlcall.baseUrl}/api/category/interiorCategoryAdd`;
    static interiorSubcategory = `${Urlcall.baseUrl}/api/subcategory/interiorSubcategoryAdd`;
    static fetchInteriorcategory =`${Urlcall.baseUrl}/api/category/fetchInteriorCategory`;
    static fetchInteriorsubcategory = `${Urlcall.baseUrl}/api/subcategory/fetch?category_id=`;
    static fetchInteriorProduct = `${Urlcall.baseUrl}/api/product/fetchInteriorProduct`;
    static fetchInteriorProductBySubcategory = `${Urlcall.baseUrl}/api/product/getinteriorproductbySubcategoryid?subCategory_id=`;
   
}
    
