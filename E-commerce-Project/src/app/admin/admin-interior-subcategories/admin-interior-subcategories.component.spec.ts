import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminInteriorSubcategoriesComponent } from './admin-interior-subcategories.component';

describe('AdminInteriorSubcategoriesComponent', () => {
  let component: AdminInteriorSubcategoriesComponent;
  let fixture: ComponentFixture<AdminInteriorSubcategoriesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminInteriorSubcategoriesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminInteriorSubcategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
