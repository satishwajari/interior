import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from '../../services/data.service';
import { Globals } from '../../services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Urlcall } from 'src/app/services/urll'; 
import { product } from '../../models/product';



@Component({
  selector: 'app-adminproduct',
  templateUrl: './adminproduct.component.html',
  styleUrls: ['./adminproduct.component.scss']
})
export class AdminproductComponent implements OnInit {
  product = new product;
  data: any;
  imageData: any;
  categoryData: any;
  nameData: any;
  categoryNameData: any;
  options: any;


  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }


  ngOnInit(): void {
    this.category();
    this.subcategory();
  }
  runMyAPI(){
    this.spinner.show();
     this.dataService.post(Urlcall.Addproduct, this.product).subscribe(res=>{
       this.spinner.hide();
       this.toastr.success(res['message'])
       if(res['status']){
         console.log(res);
         let temp = res['data'];
         
         }
        
       
     },err=>{ this.spinner.hide();
      this.toastr.success(err['message'])})
   }
   category(){
    this.spinner.show();
    this.dataService.get(Urlcall.fetchAllCateogary).subscribe(res=>{
      this.spinner.hide();
      this.options = res['data'];
    },err=>{ this.spinner.hide();})
  }

  subcategory(){
    this.spinner.show();
    this.dataService.get(Urlcall.fetchAllSubCat+this.product.category_id).subscribe(res=>{
      this.spinner.hide();
      this.data = res['data'];
    },err=>{ this.spinner.hide();})
  }

   function (url: any) {
    this.router.navigate(['/',url])
  }
}



  

