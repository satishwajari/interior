import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from 'src/app/services/data.service'; 
import { Globals } from 'src/app/services/globalvariable'; 
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Urlcall } from 'src/app/services/urll';
import { reguser } from 'src/app/models/user-reg';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { bankdetail } from '../../models/bankdetail';

export class address{
  _id:any;
  city: any;
  state: any;
  address_line1: any;
  address_line2: any;
  pinCode: any;
}

@Component({
  selector: 'app-saper-profile',
  templateUrl: './saper-profile.component.html',
  styleUrls: ['./saper-profile.component.scss'],
})
export class SaperProfileComponent implements OnInit {
  user = new reguser;
  editProfile: boolean;
  sellerAddress = new address;
 bankdetail: boolean=false;
  data: any;
  readonlyDetails: boolean=true;
  userBankDetails = new bankdetail;
  constructor(
    public cookieService: CookieService,
    private router: Router,
    private spinner: NgxSpinnerService,
    public dataService: DataService,
    public globals: Globals,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.globals.id = this.cookieService.get('id');
    this.getdata();
    this.getBank();
   
  }
  getdata() {
    this.spinner.show();
    this.dataService.get(Urlcall.fetchUserById + this.globals.id).subscribe(
      (res) => {
        this.spinner.hide();
        let temp = res['data'];
        this.user = temp[0];
        this.sellerAddress = temp[0]
      },
      (err) => {
        this.spinner.hide();
      }
    );
  }
  updateDetail(){
    this.spinner.show();
    this.dataService.put(Urlcall.userupdate,this.user).subscribe(res=>{
     this.spinner.hide();
     this.toastr.success(res['message']);
     if(res['status']){
       this.editProfile = false;
     }
   },err=>{ this.spinner.hide();this.toastr.warning(err['message'])})
  }



  Editprofile() {
    //  this.router.navigate(['/', 'Edit_Profile'])
    this.editProfile= true;
  }
addbank(){
  this.bankdetail=!this.bankdetail;
  
 
}
EditBankDetail(){
  this.readonlyDetails= !this.readonlyDetails;

}



  percent: number;
  uploadSuccessuid: boolean;
  fileData: File = null;
  previewUrl:any ;
  fileUploadProgress: string = null;
  uploadedFilePath: string = null;
  
  fileProgress(fileInput: any) {
    this.fileData = <File>fileInput.target.files[0];
    this.uploadprofileImage();
  }

uploadprofileImage(){
  this.spinner.show();
  const formData = new FormData();
    formData.append('_id',this.globals.id);
   
    if(this.fileData){
      formData.append('files', this.fileData,this.fileData.name);
    }
this.dataService.putFile(Urlcall.userupdate,formData).subscribe(res=>{
  if (res.type === HttpEventType.UploadProgress) {
      this.percent = Math.round(100 * res.loaded / res.total);
  } else if (res instanceof HttpResponse) {
    this.uploadSuccessuid = true;
    this.toastr.success(res['message']);
    this.spinner.hide();
     this.getdata();
  }
},err=>{this.toastr.warning(err['message']); this.spinner.hide();})
}


saveAddress(){
  this.spinner.show();
  this.dataService.put(Urlcall.userupdate,this.sellerAddress).subscribe(res=>{
   this.spinner.hide();
   this.toastr.success(res['message']);
   if(res['status']){
     this.editProfile = false;
   }
 },err=>{ this.spinner.hide();this.toastr.warning(err['message'])})

}


getBank(){
  this.spinner.show();
  this.dataService.get(Urlcall.fetchBankDetail+this.globals.id).subscribe(res=>{
    this.spinner.hide();
    if(res['status']){
      this.userBankDetails = res['data'];
    }
   
  },err=>{ this.spinner.hide();this.toastr.warning(err['message'])})
}


updateBank(){
  this.spinner.show();
  this.userBankDetails.user_id = this.globals.id;
  this.dataService.put(Urlcall.updateBankDetail,this.userBankDetails).subscribe(res=>{
   this.spinner.hide();
   this.userBankDetails = res['data'];
   this.toastr.success(res['message']);
   this.getBank();

   
 },err=>{ this.spinner.hide();this.toastr.warning(err['message'])})
}

}
