import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminUserorderListComponent } from './admin-userorder-list.component';

describe('AdminUserorderListComponent', () => {
  let component: AdminUserorderListComponent;
  let fixture: ComponentFixture<AdminUserorderListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminUserorderListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminUserorderListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
