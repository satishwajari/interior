import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { Globals } from '../services/globalvariable';
import { DataService } from '../services/data.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {

  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }


  ngOnInit(): void {
  }

}
