import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSellerProductlistComponent } from './admin-seller-productlist.component';

describe('AdminSellerProductlistComponent', () => {
  let component: AdminSellerProductlistComponent;
  let fixture: ComponentFixture<AdminSellerProductlistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminSellerProductlistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSellerProductlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
