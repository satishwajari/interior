import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookConsultantComponent } from './book-consultant.component';

describe('BookConsultantComponent', () => {
  let component: BookConsultantComponent;
  let fixture: ComponentFixture<BookConsultantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookConsultantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookConsultantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
