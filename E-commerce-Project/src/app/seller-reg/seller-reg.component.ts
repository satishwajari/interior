
import { Component, OnInit } from '@angular/core';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';
import { NgxSpinnerModule, NgxSpinnerService } from "ngx-spinner";
import { DataService } from '../services/data.service';
import { Router } from '@angular/router';
import { selrguser} from '../models/selr-reg';
import { Globals } from '../services/globalvariable';
import { Urlcall } from '../services/urll';




@Component({
  selector: 'app-seller-reg',
  templateUrl: './seller-reg.component.html',
  styleUrls: ['./seller-reg.component.scss']
})
export class SellerRegComponent implements OnInit {
user = new selrguser;
  
  constructor(public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }



  ngOnInit(): void {
  }
  next(url){
    this.router.navigate(['/',url])
  }

  runMyAPI(){ 
    this.spinner.show();
     this.dataService.post(Urlcall.sellerReg,this.user).subscribe(res=>{
       this.spinner.hide();
       this.toastr.success(res['message'])
       if(res['status']){
         console.log(res);
         this.globals.temp_id= res['data']._id;
        //  this.globals.role = res['data'].role;
         this.cookieService.set('temp_id',this.globals.temp_id);
        //  this.cookieService.set('role',this.globals.role);
         
         
          this.next('otp');
         }
        
       
     },err=>{ this.spinner.hide();
      this.toastr.success(err['message'])})
   }
  seller(Reg: any, seller: any) {
    throw new Error('Method not implemented.');
    
  }
}

