import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from 'src/app/services/data.service';
import { Globals } from 'src/app/services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Urlcall } from '../services/urll';
import { reguser } from '../models/user-reg';
import { address } from '../models/address';




@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
})
export class UserProfileComponent implements OnInit {
  user = new reguser;
  editProfile: boolean;
  EditAddress = new address;


  constructor(
    public cookieService: CookieService,
    private router: Router,
    private spinner: NgxSpinnerService,
    public dataService: DataService,
    public globals: Globals,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.globals.id = this.cookieService.get('id');
    this.getdata();
   
  }
  getdata() {
    this.spinner.show();
    this.dataService.get(Urlcall.fetchUserById + this.globals.id).subscribe(
      (res) => {
        this.spinner.hide();
        let temp = res['data'];
        this.user = temp[0] ;
      },
      (err) => {
        this.spinner.hide();
      }
    );
  }

  



  Editprofile() {
    //  this.router.navigate(['/', 'Edit_Profile']);
    this.editProfile = true
   
  }
  updateDetail(){
    this.spinner.show();
    this.dataService.put(Urlcall.userupdate,this.user).subscribe(res=>{
     this.spinner.hide();
     this.toastr.success(res['message']);
     if(res['status']){
       this.editProfile = false;
     }
   },err=>{ this.spinner.hide();this.toastr.warning(err['message'])})
  }


edituser(){
this.router.navigate(['/','edituseradress'])
}

fogetpass(){
  this.router.navigate(['/','password'])
}
logout(){
  this.cookieService.deleteAll();
  window.location.reload();
    
  
}

}
