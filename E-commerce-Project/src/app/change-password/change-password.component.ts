import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { Globals } from '../services/globalvariable';
import { DataService } from '../services/data.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Urlcall } from '../services/urll';
import { changepassword } from '../models/change-pwd';


@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

 cPwd = new changepassword;

  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }


  ngOnInit(): void {
    this.globals.id = this.cookieService.get('id');
     this.cPwd.userId = this.globals.id;
    if (this.globals.id == ""){
      this.router.navigate(['/']);
    }
  }

changePwd(){ 
    this.spinner.show();
      this.dataService.put(Urlcall.changepassword, this.cPwd).subscribe(res=>{
        this.spinner.hide(); 
       this.toastr.success(res['message'])
        if(res['status']){
          console.log(res);
          this.logout();
         }
      },err=>{ this.spinner.hide();
       this.toastr.success(err['message'])})
    }

    
      logout(){
        this.cookieService.deleteAll();
        this.router.navigate(['/','login']);
        window.location.reload();
      }
    

    cancel(){

    }
  
 }
