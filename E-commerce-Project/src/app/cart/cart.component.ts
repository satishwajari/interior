import { Component, OnInit } from '@angular/core';

import { CookieService } from 'ngx-cookie-service';
import { Globals } from '../services/globalvariable';
import { DataService } from '../services/data.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import {NgbModal, ModalDismissReasons,NgbModalModule} from '@ng-bootstrap/ng-bootstrap';

import { Urlcall } from '../services/urll';
import { address } from '../models/address';

export class transportdata {
  totalTransaportingCharge: any;
  totalDiscountPrice: any;
  totalPrice: any;
  currentImage: any;
}


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})


export class CartComponent implements OnInit {

  NgbdModalBasic 
  closeResult = '';

  data: any;
  selectedAddress:any;
  totalAmmount: number;
  dataAddress: address[];
  Address = new address;
  Amount  = new transportdata;
  datamodel: NgbModalModule;
  calAmount: any;
  EditAddress = new address;
 
 
  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService ) { }

  ngOnInit(): void {
    this.globals.id = this.cookieService.get('id');
    if(this.globals.id == ""){
      this.router.navigate(['/','signin'])
    }
    this.getdata();
    this.getdataAddress();
    this.globals.BuyNowproduct_id = null;
    this.cookieService.delete('BuyNowproduct_id');
  }
    
  getdata(){
    this.totalAmmount=0;
    this.spinner.show();
    this.dataService.get(Urlcall.fetchcart+this.globals.id).subscribe(res=>{
      this.spinner.hide();
      this. data= res['data'];
      for(let i=0; i< this.data.length; i++){
        this.totalAmmount= this.totalAmmount+ (Number(this.data[i].product_id.price) * Number(this.data[i].quantity));
      }
      
     
    },err=>{ this.spinner.hide();})
  }

  

  updateQuantity(id,quantity){
   console.log('quantity',quantity)
   
      let data ={
        _id: id,
        quantity: quantity
      }
    this.dataService.put(Urlcall.updateQuantity,data).subscribe(res=>{
     this.spinner.hide();
     this.toastr.success(res['message']);
     this.TransportCharge();
   },err=>{ this.spinner.hide();this.toastr.warning(err['message'])})

  }
   
  


  

  RemoveCart(id){
    this.spinner.show();
    this.dataService.delete(Urlcall.deletecart+id).subscribe(res=>{
      this.spinner.hide();
      this.getdata();
      this.TransportCharge();
      this.toastr.success(res['message'])
    },err=>{this.spinner.hide();
      this.toastr.warning(err['message'])
    })
  }

  placeorder(){ 
    if(this.globals.address_id){
      this.router.navigate(['/','User-Payment'])
    }else{
      this.toastr.warning("please select address first")
    }
  
    }
    
    addAddress(){
      this.Address.user_id = this.globals.id;
      this.spinner.show();
      this.dataService.post(Urlcall.AddDeliveryAddress,this.Address).subscribe(res=>{
        this.toastr.success(res['message']);
        this.spinner.hide();
        this.modalService.dismissAll();
        if(res['status']){
          this.Address = new address;
          this.getdataAddress();
        }
      },err=>{
        this.toastr.warning(err['message']);
        this.spinner.hide();
      })
    }
  
    getdataAddress(){
      this.spinner.show();
      this.dataService.get(Urlcall.FetchdeliveryAddress+this.globals.id).subscribe(res=>{
        this.dataAddress = res['data'];
        if(res['status']){
          this.ChangeAddress(this.dataAddress[0]);
        }
        
        this.spinner.hide();
      },err=>{this.spinner.hide();})
    }
    // getdataTranport(){
    //   this.spinner.show();
    //   this.dataService.get(Urlcall.fetchTransportCharges+this.globals.id+"&_id="+this.globals.address_id).subscribe(res=>{
    //     this.spinner.hide();
    //     this.Amount= res['data'];
    //   },err=>{ this.spinner.hide();})
    // }
    TransportCharge(){ 
      {
      let data={
        
          user_id: this.globals.id,
          _id:  this.globals.address_id
        }
      
     this.spinner.show();
      this.dataService.post(Urlcall.transportCharge,data).subscribe(res=>{
        this.spinner.hide();
         this.calAmount = res['data'];
         
    },err=>{ this.spinner.hide();})
    }
  }
  



    ChangeAddress(rel){
      console.log('rel',rel);
      this.selectedAddress = rel;
      this.globals.address_id = rel._id;
      this.cookieService.set("address_id",this.globals.address_id)
      // this. getdataTranport()
      this.TransportCharge()
      
    }

    productview(id){
      this.globals.product_id =id;
      this.cookieService.set('product_id',this.globals.product_id);
      console.log('productid',id);
      this.router.navigate(['/Product-View'] ,{ queryParams: { id : id}});   
    }


    RemoveAddress(id){
      this.spinner.show();
      this.dataService.delete(Urlcall.deleteAddress+id).subscribe(res=>{
        this.spinner.hide();
        this.getdataAddress();
        this.toastr.success(res['message'])
      },err=>{this.spinner.hide();
        this.toastr.warning(err['message'])
      })
    }

    open(content) {
     this.datamodel = this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    }
  
    private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
      } else {
        return `with: ${reason}`;
      }
    }
  

    updateAddress(){
      this.spinner.show()
    this.dataService.put(Urlcall.updateAddress,this.EditAddress).subscribe(res=>{
     this.spinner.hide();
     this.getdataAddress();
     this.toastr.success(res['message']);
     
     
   },err=>{ this.spinner.hide();this.toastr.warning(err['message'])})

  }

  edit(rel){
    this.EditAddress =rel;
    
    console.log(this.EditAddress);
  } 

}
