import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { Globals } from '../services/globalvariable';
import { DataService } from '../services/data.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Urlcall } from '../services/urll';
import { reguser } from '../models/user-reg';


@Component({
  selector: 'app-edit-user-profile',
  templateUrl: './edit-user-profile.component.html',
  styleUrls: ['./edit-user-profile.component.scss']
})
export class EditUserProfileComponent implements OnInit {
  user = new reguser;
  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }



    ngOnInit(): void {
      this.globals.id = this.cookieService.get('id');
      this.getdata();
    }
  getdata(){
    this.spinner.show();
    this.dataService.get(Urlcall.fetchUserById+this.globals.id).subscribe(res=>{
      this.spinner.hide();
      this.user = res['data'];
    },err=>{this.spinner.hide();})
  }

  
  
}
