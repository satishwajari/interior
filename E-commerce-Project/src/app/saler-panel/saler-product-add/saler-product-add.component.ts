import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from '../../services/data.service';
import { Globals } from '../../services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Urlcall } from 'src/app/services/urll'; 
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { product } from '../../models/product';
import { HttpEventType, HttpResponse } from '@angular/common/http';



@Component({
  selector: 'app-saler-product-add',
  templateUrl: './saler-product-add.component.html',
  styleUrls: ['./saler-product-add.component.scss']
})
export class SalerProductAddComponent implements OnInit {
  product = new product;
  data: any;
  imageData: any;
  categoryData: any;
  nameData: any;
  categoryNameData: any;
  options: any;
  summery:any;



  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }

    ngOnInit(): void {
      this.globals.id = this.cookieService.get('id');
      this.globals.role = this.cookieService.get('role');
      if(this.globals.id == null){
        this.router.navigate(['/','/'])
      }
      this.category();
      // this.subcategory();
    }
    productAPI(){
      this.spinner.show();
       this.dataService.post(Urlcall.Addproduct, this.product).subscribe(res=>{
         this.spinner.hide();
         this.toastr.success(res['message'])
         if(res['status']){
           console.log(res);
           let temp = res['data'];
           
           }
          
         
       },err=>{ this.spinner.hide();
        this.toastr.success(err['message'])})
     }
     category(){
      this.spinner.show();
      
      this.dataService.get(Urlcall.fetchAllCateogary).subscribe(res=>{
        this.spinner.hide();
        this.options = res['data'];
      },err=>{ this.spinner.hide();})
    }
  
    subcategory(){
      this.spinner.show();
      this.data = null ;
      this.product.subCategory_id = null;
      this.dataService.get(Urlcall.fetchAllSubCat+this.product.category_id).subscribe(res=>{
        this.spinner.hide();
        this.data = res['data'];
      },err=>{ this.spinner.hide();})
    }
  
     function(url: any) {
      this.router.navigate(['/',url])
    }



    percent: number;
    uploadSuccessuid: boolean;
    fileData: File = null;
    previewUrl:any ;
    fileUploadProgress: string = null;
    uploadedFilePath: string = null;
    
    fileProgress(fileInput: any) {
      this.fileData = <File>fileInput.target.files[0];
    }
  
  uploadFile(){
    this.spinner.show();
    const formData = new FormData();
      formData.append('seller_id',this.globals.id);
      formData.append('category_id',this.product.category_id);
      formData.append('price',this.product.price);
      formData.append('subCategory_id',this.product.subCategory_id);
      formData.append('productName',this.product.productName);
      formData.append('summery',this.product.summery);
      // formData.append('product_selling_price',this.product.product_selling_price);
      // formData.append('product_price_parcentage',this.product.product_price_parcentage);
      formData.append('product_discount_parcentage',this.product.product_discount_parcentage);
      formData.append('item_height',this.product.item_height);
      formData.append('item_length',this.product.item_length);
      formData.append('item_breadth',this.product.item_breadth);
      formData.append('item_weight',this.product.item_weight);
      formData.append('shipping_charge',this.product.shipping_charge);
      

      if(this.fileData){
        formData.append('files', this.fileData,this.fileData.name);
      }
  this.dataService.postFile(Urlcall.Addproduct,formData).subscribe(res=>{
    console.log(res);
    if (res.type === HttpEventType.UploadProgress) {
        this.percent = Math.round(100 * res.loaded / res.total);
    } else if (res instanceof HttpResponse) {
      this.uploadSuccessuid = true;
      this.toastr.success(res['message']);
      this.spinner.hide();
      // this.getdata();
      this.router.navigate(['/','seller-product-list'])
    }
  },err=>{this.toastr.warning(err['message']); this.spinner.hide();})
  }

  editorConfig: AngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: '200',
      minHeight: '200',
      maxHeight: 'auto',
      width: '10',
      minWidth: '20',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        {class: 'arial', name: 'Arial'},
        {class: 'times-new-roman', name: 'Times New Roman'},
        {class: 'calibri', name: 'Calibri'},
        {class: 'comic-sans-ms', name: 'Comic Sans MS'}
      ],
      customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    //upload: (file: File) => { ... }
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      ['bold', 'italic'],
      ['fontSize']
    ]
};




  }
  
  
  
    
  
  
 