import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCatgoriesComponent } from './admin-catgories.component';

describe('AdminCatgoriesComponent', () => {
  let component: AdminCatgoriesComponent;
  let fixture: ComponentFixture<AdminCatgoriesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminCatgoriesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCatgoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
