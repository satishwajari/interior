import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from 'src/app/services/data.service';
import { Globals } from 'src/app/services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Urlcall } from 'src/app/services/urll';
import { reguser } from 'src/app/models/user-reg';
import { address } from 'src/app/models/address';


@Component({
  selector: 'app-admin-user-list',
  templateUrl: './admin-user-list.component.html',
  styleUrls: ['./admin-user-list.component.scss']
})
export class AdminUserListComponent implements OnInit {
  data: any[];
  status:any;
  userAddress= new address;
  user= new reguser;
  editProfile:boolean;
  page=1;
  userid:any;
  addressData:any;

  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.globals.id = this.cookieService.get('id');
    this.getdata('pending',1);
  }
  getdata(status,page){
    this.status = status;
    console.log(status);
    this.spinner.show();
      this.dataService.get(Urlcall.fetchUser+'&status='+status+ '&page='+page).subscribe(res=>{
       this.spinner.hide();
       this.data = res['data'];
     },err=>{ this.spinner.hide();})
  }
   deleteuser(id){
     this.spinner.show();
     this.dataService.delete(Urlcall.userdelete+id).subscribe(res=>{
       this.spinner.hide();
       this.getdata('pending',this.page);
       this.toastr.success(res['message'])
   },err=>{this.spinner.hide();
       this.toastr.warning(err['message'])
     })
  }
  userprofile(rel){
    this.userid = rel._id;
    this.user = rel;
    this.fetchAdressData();
  }

  
  


   
   
    fetchAdressData(){
      this.spinner.show();
      this.dataService.get(Urlcall.FetchdeliveryAddress+this.userid).subscribe(res=>{
        this. addressData = res['data'];
        this.spinner.hide();
        
      },err=>{this.spinner.hide();})
    }



}
