import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataService } from '../../services/data.service';
import { Globals } from '../../services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { HttpResponse, HttpEventType } from '@angular/common/http';
import {
  NgbModal,
  ModalDismissReasons,
  NgbModalRef,
} from '@ng-bootstrap/ng-bootstrap';
import { interiorcategory } from 'src/app/models/interior-category';
import { interiorproduct } from 'src/app/models/interior-product';
import { Urlcall } from '../../services/urll';

@Component({
  selector: 'app-admin-interior-categories',
  templateUrl: './admin-interior-categories.component.html',
  styleUrls: ['./admin-interior-categories.component.scss']
})
export class AdminInteriorCategoriesComponent implements OnInit {
  data:any[];
  categoryName:any;
  interiorcategory = new interiorcategory;
  interiorproduct = new interiorproduct;

  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }

  ngOnInit(): void {
     this.getdata();
  }
  openModel(fileUpload){
    this.modalService.open(fileUpload, { centered: true });
  }
   fileData: File = null;
   percent: number;
   uploadSuccessuid: boolean;
   uploadedFilePath: string = null;

  fileProgress(fileInput: any) {
    this.fileData = <File>fileInput.target.files[0];
  }

  uploadFile(){
    this.spinner.show();
    const formData = new FormData();
      formData.append('categoryName',this.categoryName );
      if(this.fileData){
        formData.append('files', this.fileData,this.fileData.name);
      }
  this.dataService.postFile(Urlcall.interiorCategory,formData).subscribe(res=>{
    console.log(res);
    if (res.type === HttpEventType.UploadProgress) {
        this.percent = Math.round(100 * res.loaded / res.total);
    } else if (res instanceof HttpResponse) {
      this.uploadSuccessuid = true;
      this.toastr.success(res['message']);
      this.spinner.hide();
        this.getdata();
    }
  },err=>{this.spinner.hide()})
  }

 getdata(){
  this.spinner.show();
  this.dataService.get(Urlcall.fetchInteriorcategory).subscribe(res=>{
    this.spinner.hide();
    this.data = res['data'];
  },err=>{ this.spinner.hide();})
 } 

 addSub(id){
  this.globals.CatId = id;
  this.router.navigate(['/','admin-interior-subcategory']);
  }
  
  deleteproduct(id){
    this.spinner.show();
    this.dataService.delete(Urlcall.deletecategory+id).subscribe(res=>{
      this.spinner.hide();
      this.getdata();
      this.toastr.success(res['message'])
    },err=>{this.spinner.hide();
      this.toastr.warning(err['message'])
    })
  }
  updateCategory(){
    this.spinner.show();
    const formData = new FormData();
    formData.append('_id',this.interiorcategory ._id );
    formData.append('categoryName',this.interiorcategory .categoryName );
      if(this.fileData){
        formData.append('files', this.fileData,this.fileData.name);
      }
  this.dataService.putFile(Urlcall.updateCategory,formData).subscribe(res=>{
    console.log(res);
    if (res.type === HttpEventType.UploadProgress) {
        this.percent = Math.round(100 * res.loaded / res.total);
    } else if (res instanceof HttpResponse) {
      this.uploadSuccessuid = true;
      this.toastr.success(res['message']);
      this.spinner.hide();
      this.getdata();
    }
  },err=>{this.spinner.hide()})
  }
  editcat(categorydata){
    this.interiorcategory  = categorydata;
  }
}
