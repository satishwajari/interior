import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from '../../services/data.service';
import { Globals } from '../../services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { interiorproduct } from 'src/app/models/interior-product';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { Urlcall } from '../../services/urll';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import {NgbModal, ModalDismissReasons,NgbModalModule} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-interior-product',
  templateUrl: './interior-product.component.html',
  styleUrls: ['./interior-product.component.scss']
})
export class InteriorProductComponent implements OnInit {

  NgbdModalBasic 
  closeResult = '';
  interiorproduct = new interiorproduct;
  data: any;
  options:any;
  currentImage: any;
  datamodel: NgbModalModule;
  summery:any;
  subcategorydata: any; 
 

  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.globals.id = this.cookieService.get('id');
    this.globals.role = this.cookieService.get('role');
     this.getdata();
    if(this.globals.id == null){
      this.router.navigate(['/','/'])
    }
    this.interiorcategory();
  }
  percent: number;
  uploadSuccessuid: boolean;
  fileData: File = null;
  previewUrl:any ;
  fileUploadProgress: string = null;
  uploadedFilePath: string = null;
  
  fileProgress(fileInput: any) {
    this.fileData = <File>fileInput.target.files[0];
    this.addImage();
  }

  productAPI(){
    this.spinner.show();
     this.dataService.post(Urlcall.interiorProduct, this.interiorproduct).subscribe(res=>{
       this.spinner.hide();
       this.toastr.success(res['message'])
       if(res['status']){
         console.log(res);
         let temp = res['data'];
         
         }
        
       
     },err=>{ this.spinner.hide();
      this.toastr.success(err['message'])})
   }
   interiorcategory(){
    this.spinner.show();
    
     this.dataService.get(Urlcall.fetchInteriorcategory).subscribe(res=>{
       this.spinner.hide();
        this.options = res['data'];
     },err=>{ this.spinner.hide();})
   }
   subcategory(){
    this.spinner.show();
    this.data = null ;
    this.interiorproduct.subCategory_id = null;
    this.dataService.get(Urlcall.fetchInteriorsubcategory+this.interiorproduct.category_id).subscribe(res=>{
      this.spinner.hide();
      this.data = res['data'];
    },err=>{ this.spinner.hide();})
  }
  function(url: any) {
    this.router.navigate(['/',url])
  }
  
   uploadFile(){
    this.spinner.show();
    const formData = new FormData();
    formData.append('category_id',this.interiorproduct.category_id);
    formData.append('subCategory_id',this.interiorproduct.subCategory_id);
    formData.append('productName',this.interiorproduct.productName);
    formData.append('summery',this.interiorproduct.summery);
    if(this.fileData){
      formData.append('files', this.fileData,this.fileData.name);
    }
this.dataService.postFile(Urlcall.interiorProduct,formData).subscribe(res=>{
  console.log(res);
  if (res.type === HttpEventType.UploadProgress) {
      this.percent = Math.round(100 * res.loaded / res.total);
  } else if (res instanceof HttpResponse) {
    this.uploadSuccessuid = true;
    this.toastr.success(res['message']);
    this.spinner.hide();
    // this.getdata();
     this.router.navigate(['/','interior-product'])
  }
},err=>{this.toastr.warning(err['message']); this.spinner.hide();})
}

getdata(){
  this.spinner.show();
    this.dataService.get(Urlcall.fetchInteriorProduct).subscribe(res=>{
      this.spinner.hide();

      this.data = res['data'];
      
    },err=>{this.spinner.hide();})
}

 deleteproduct(id){
   this.spinner.show();
   this.dataService.delete(Urlcall.deleteproduct+id).subscribe(res=>{
     this.spinner.hide();
     this.getdata();
     this.toastr.success(res['message'])
   },err=>{this.spinner.hide();
     this.toastr.warning(err['message'])
   })
 }
 update(){
  this.spinner.show();
  this.dataService.put(Urlcall.updateProduct,this.interiorproduct).subscribe(res=>{
   this.spinner.hide();
   this.modalService.dismissAll();
   this.toastr.success(res['message']);
   this.getdata()

   
 },err=>{ this.spinner.hide();this.toastr.warning(err['message'])})
}
edit(rel){
     this. interiorproduct =rel;
       this.currentImage = this.interiorproduct.image[0].img_name;
    console.log(this. interiorproduct);
   }

  open(content) {
   this.datamodel = this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
       this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
       this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   }
   private getDismissReason(reason: any): string {
     if (reason === ModalDismissReasons.ESC) {
       return 'by pressing ESC';
     } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
       return 'by clicking on a backdrop';
     } else {
       return `with: ${reason}`;
    }
   }
   addImage(){
    this.spinner.show();
    const formData = new FormData();
      formData.append('_id',this.interiorproduct._id);    
  
      if(this.fileData){
        formData.append('files', this.fileData,this.fileData.name);
      }
  this.dataService.putFile(Urlcall.updateIMAGE,formData).subscribe(res=>{
    console.log(res);
    if (res.type === HttpEventType.UploadProgress) {
        this.percent = Math.round(100 * res.loaded / res.total);
    } else if (res instanceof HttpResponse) {
      this.uploadSuccessuid = true;
      this.toastr.success(res['message']);
      this.spinner.hide();
       this.getdata();
      //this.router.navigate(['/','seller-product-list'])
    }
  },err=>{this.toastr.warning(err['message']); this.spinner.hide();})
  }
  
    deleteImage(id){
     this.spinner.show();
     this.dataService.delete(Urlcall.deleteImage+id+'&product_id='+this.interiorproduct._id).subscribe(res=>{
       this.spinner.hide();
       this.getdata();
       this.toastr.success(res['message'])
     },err=>{this.spinner.hide();
       this.toastr.warning(err['message'])
     })
   }
  selectImg(img){
    this.currentImage = img;
  } 

  

editorConfig: AngularEditorConfig = {
  editable: true,
    spellcheck: true,
    height: '200',
    minHeight: '200',
    maxHeight: 'auto',
    width: '10',
    minWidth: '20',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      {class: 'arial', name: 'Arial'},
      {class: 'times-new-roman', name: 'Times New Roman'},
      {class: 'calibri', name: 'Calibri'},
      {class: 'comic-sans-ms', name: 'Comic Sans MS'}
    ],
    customClasses: [
    {
      name: 'quote',
      class: 'quote',
    },
    {
      name: 'redText',
      class: 'redText'
    },
    {
      name: 'titleText',
      class: 'titleText',
      tag: 'h1',
    },
  ],
  uploadUrl: 'v1/image',
  //upload: (file: File) => { ... }
  uploadWithCredentials: false,
  sanitize: true,
  toolbarPosition: 'top',
  toolbarHiddenButtons: [
    ['bold', 'italic'],
    ['fontSize']
  ]
};

}

  