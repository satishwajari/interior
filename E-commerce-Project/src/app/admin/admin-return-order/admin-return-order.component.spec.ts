import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminReturnOrderComponent } from './admin-return-order.component';

describe('AdminReturnOrderComponent', () => {
  let component: AdminReturnOrderComponent;
  let fixture: ComponentFixture<AdminReturnOrderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminReturnOrderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminReturnOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
