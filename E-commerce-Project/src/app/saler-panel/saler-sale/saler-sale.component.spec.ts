import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalerSaleComponent } from './saler-sale.component';

describe('SalerSaleComponent', () => {
  let component: SalerSaleComponent;
  let fixture: ComponentFixture<SalerSaleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalerSaleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalerSaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
