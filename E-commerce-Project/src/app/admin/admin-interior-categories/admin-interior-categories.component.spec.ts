import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminInteriorCategoriesComponent } from './admin-interior-categories.component';

describe('AdminInteriorCategoriesComponent', () => {
  let component: AdminInteriorCategoriesComponent;
  let fixture: ComponentFixture<AdminInteriorCategoriesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminInteriorCategoriesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminInteriorCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
