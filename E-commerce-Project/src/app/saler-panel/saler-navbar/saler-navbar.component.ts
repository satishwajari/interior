import { Component, OnInit } from '@angular/core';
import { Globals } from 'src/app/services/globalvariable';

@Component({
  selector: 'app-saler-navbar',
  templateUrl: './saler-navbar.component.html',
  styleUrls: ['./saler-navbar.component.scss']
})
export class SalerNavbarComponent implements OnInit {
  sidenav: boolean;

  constructor(
    public globals: Globals,
  ) { }

  ngOnInit(): void {
  }
  sidenavClick(){
    this.globals.sidenav = !this.globals.sidenav; 
  }
}
