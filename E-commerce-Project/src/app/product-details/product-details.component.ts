import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from '../services/data.service';
import { Globals } from '../services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Urlcall } from '../services/urll';
import { product } from '../models/product';


@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {
  data= new product;
  currentImage: any;
  categoryData: any;
  productNameData: any;
  subCategoryData: any;
 
  

  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,
    private spinner: NgxSpinnerService,private activatedRoute: ActivatedRoute,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { 

      this.activatedRoute.queryParams.subscribe(params => {
        let id = params['id'];
       this.globals.product_id = id;
       this.getdata();
       
    });

    }


  ngOnInit(): void {
    // this.globals.product_id =this.cookieService.get('product_id')
    // this.getdata(); 
  }
  getdata(){
    this.spinner.show();
    this.dataService.get(Urlcall.fetchproductData+this.globals.product_id).subscribe(res=>{
      this.spinner.hide();
      const temp=res['data'];
      this.data = temp[0];
      this.productNameData = this.data['productName']
      this.categoryData = this.data['category']
      this.subCategoryData = this.data['subCategory']
      this.currentImage = this.data.image[0].img_name;

    },err=>{ this.spinner.hide();})
  }
  buynow(){
    if(this.globals.id){
     this.globals.BuyNowproduct_id = this.globals.product_id;
     this.cookieService.set('BuyNowproduct_id',this.globals.BuyNowproduct_id);
     this.router.navigate(['/','Address']);
    }else{
      this.router.navigate(['/signin'])
    }
  }

  addcart(id){ 
    if(this.globals.id){
    let data={
      user_id: this.globals.id,
      product_id: id
    }
   this.spinner.show();
    this.dataService.post(Urlcall.cartAdd,data).subscribe(res=>{
      this.spinner.hide();
       this.data = res['data'];
       this.router.navigate(['/','Cart'])
  },err=>{ this.spinner.hide();})
  }else{
    this.router.navigate(['/signin'])
  }
}

selectImage(imageLink){
  this.currentImage = imageLink;
}


}



