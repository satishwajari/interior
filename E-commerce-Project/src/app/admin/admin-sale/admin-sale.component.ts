import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataService } from 'src/app/services/data.service';
import { Globals } from 'src/app/services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { Urlcall } from 'src/app/services/urll';

@Component({
  selector: 'app-admin-sale',
  templateUrl: './admin-sale.component.html',
  styleUrls: ['./admin-sale.component.scss']
})
export class AdminSaleComponent implements OnInit {
  
  data: any;

  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }



  ngOnInit(): void {

    this.globals.id = this.cookieService.get('id');
   this.getdata();
  }
  getdata(){
    this.spinner.show();
    this.dataService.get(Urlcall.fetchAdminSale,).subscribe(res=>{
      this.spinner.hide();
      this.data = res['data'];
    },err=>{ this.spinner.hide();})
  }

  }


