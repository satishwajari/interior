import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataService } from '../../services/data.service';
import { Globals } from '../../services/globalvariable';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-saler-sidenavbar',
  templateUrl: './saler-sidenavbar.component.html',
  styleUrls: ['./saler-sidenavbar.component.scss']
})
export class SalerSidenavbarComponent implements OnInit {
  sidenav: boolean;
  constructor(public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.globals.id = this.cookieService.get('id');
    this.globals.role = this.cookieService.get('role');
    if(this.globals.role != "seller"){
      this.router.navigate(['/','/']);
    }
  }

  logout(){
    this.cookieService.deleteAll();
    window.location.reload();
  }
  sidenavClick(){
    this.globals.sidenav = !this.globals.sidenav; 
  }
}
