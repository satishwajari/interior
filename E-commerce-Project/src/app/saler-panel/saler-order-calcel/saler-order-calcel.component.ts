import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from '../../services/data.service';
import { Globals } from '../../services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Urlcall } from '../../services/urll';
import { address } from 'src/app/models/address'; 




@Component({
  selector: 'app-saler-order-calcel',
  templateUrl: './saler-order-calcel.component.html',
  styleUrls: ['./saler-order-calcel.component.scss']
})
export class SalerOrderCalcelComponent implements OnInit {

  data: any;
  currentAddr = new address;
  status: any;

  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }


  ngOnInit(): void {
    this.globals.id = this.cookieService.get('id');
    this.status = 'pending';
    this.getdata(this.status);

  }
  getdata(status){
    this.status = status;
    this.spinner.show();
    this.dataService.get(Urlcall.fetchSellerAllOrder+this.globals.id+'&orderStatus='+status).subscribe(res=>{
      this.spinner.hide();
      this.data = res['data'];
    },err=>{ this.spinner.hide();})
  }

  address(addressItem){
    this.currentAddr = addressItem;
    console.log("Curent Addr : " + JSON.stringify(this.currentAddr));
  }

  updateOrder(id,status){
    this.spinner.show();
    let data ={
      order_id: id,
      orderStatus: status
      
      }
    this.dataService.put(Urlcall.updateOrder,data).subscribe(res=>{
     this.spinner.hide();
     this.toastr.success(res['message']);
     
     
   },err=>{ this.spinner.hide();this.toastr.warning(err['message'])})
}

}
