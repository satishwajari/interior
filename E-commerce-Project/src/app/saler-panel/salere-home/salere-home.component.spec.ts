import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalereHomeComponent } from './salere-home.component';

describe('SalereHomeComponent', () => {
  let component: SalereHomeComponent;
  let fixture: ComponentFixture<SalereHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalereHomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalereHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
