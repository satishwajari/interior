import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';
import { Globals } from '../../services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Urlcall } from '../../services/urll';
import { address } from '../../models/address';


@Component({
  selector: 'app-user-edit-address',
  templateUrl: './user-edit-address.component.html',
  styleUrls: ['./user-edit-address.component.scss']
})
export class UserEditAddressComponent implements OnInit {

  data: any[];
  Address = new address;
  addAddress = false;
  EditAddress = new address;

  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.globals.id = this.cookieService.get('id');
    this.getdata();
    
  }
  add(){
    this.Address.user_id = this.globals.id;
    this.spinner.show();
    this.dataService.post(Urlcall.AddDeliveryAddress,this.Address).subscribe(res=>{
      this.toastr.success(res['message']);
      this.spinner.hide();
      if(res['status']){
        this.Address = new address;
        this.addAddress = false;
        this.getdata();
      }
    },err=>{
      this.toastr.warning(err['message']);
      this.spinner.hide();
    })
  }

  getdata(){
    this.spinner.show();
    this.dataService.get(Urlcall.FetchdeliveryAddress+this.globals.id).subscribe(res=>{
      this.data = res['data'];
      this.spinner.hide();
      this.addressSelect(this.data[0]._id)
    },err=>{this.spinner.hide();})
  }

  

    addressSelect(id){
      this.globals.address_id = id;
      this.cookieService.set('address_id',this.globals.address_id);
     
    }

    

   



  RemoveAddress(id){
    this.spinner.show();
    this.dataService.delete(Urlcall.deleteAddress+id).subscribe(res=>{
      this.spinner.hide();
      this.getdata();
      this.toastr.success(res['message'])
    },err=>{this.spinner.hide();
      this.toastr.warning(err['message'])
    })
  }


  updateAddress(){
    this.spinner.show()
  this.dataService.put(Urlcall.updateAddress,this.EditAddress).subscribe(res=>{
   this.spinner.hide();
   this.getdata();
   this.toastr.success(res['message']);
   
   
 },err=>{ this.spinner.hide();this.toastr.warning(err['message'])})

}

edit(rel){
  this.EditAddress =rel;
  
  
  console.log(this.EditAddress);
} 

}
