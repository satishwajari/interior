import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common'; 
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MainareaComponent } from './mainarea/mainarea.component';
import { FooterComponent } from './footer/footer.component';
import { Img1Component } from './img1/img1.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { AboutComponent } from './about/about.component';
import { CartComponent } from './cart/cart.component';
import { UserHomeComponent } from './user-panel/user-home/user-home.component';
import { SalereHomeComponent } from './saler-panel/salere-home/salere-home.component';
import { SalerLoginComponent } from './saler-panel/saler-login/saler-login.component';
import { SalerNavbarComponent } from './saler-panel/saler-navbar/saler-navbar.component';
import { SalerSidenavbarComponent } from './saler-panel/saler-sidenavbar/saler-sidenavbar.component';
import { Img2Component } from './img2/img2.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { AllCatgoriesComponent } from './all-catgories/all-catgories.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';
import { AdminhomeComponent } from './admin/adminhome/adminhome.component';
import { AdminCatgoriesComponent } from './admin/admin-catgories/admin-catgories.component';
import { AdminproductComponent } from './admin/adminproduct/adminproduct.component';
import { AdminSubcatgoryComponent } from './admin/admin-subcatgory/admin-subcatgory.component';
import { AdminSellerlistComponent } from './admin/admin-sellerlist/admin-sellerlist.component';
import { LoginComponent } from './login/login.component';
import { UserRegistrationComponent } from './user-registration/user-registration.component';
import {HttpClientModule} from '@angular/common/http';
import { OtpComponent } from './otp/otp.component';
import { SellerRegComponent } from './seller-reg/seller-reg.component';
import { AdminNavbarComponent } from './admin/admin-navbar/admin-navbar.component';
import { AdminSideNavbarComponent } from './admin/admin-side-navbar/admin-side-navbar.component';
import { AdminFooterComponent } from './admin/admin-footer/admin-footer.component';
import { AdminProductListComponent } from './admin/admin-product-list/admin-product-list.component';
import { Globals } from './services/globalvariable';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { TermsConditinComponent } from './terms-conditin/terms-conditin.component';
import { AdminVendorListComponent } from './admin/admin-vendor-list/admin-vendor-list.component';
import { AdminSaleComponent } from './admin/admin-sale/admin-sale.component';
import { AdminCancelOrderComponent } from './admin/admin-cancel-order/admin-cancel-order.component';
import { AdminReturnOrderComponent } from './admin/admin-return-order/admin-return-order.component';
import { AdminBannerComponent } from './admin/admin-banner/admin-banner.component';
import { SalerSaleComponent } from './saler-panel/saler-sale/saler-sale.component';
import { SalerOrderCalcelComponent } from './saler-panel/saler-order-calcel/saler-order-calcel.component';
import { SalerReturnComponent } from './saler-panel/saler-return/saler-return.component';

import { SalerPurchaseReturnComponent } from './saler-panel/saler-purchase-return/saler-purchase-return.component';
import { SalerProductAddComponent } from './saler-panel/saler-product-add/saler-product-add.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SellerProductListComponent } from './seller-product-list/seller-product-list.component';
import { ProductListComponent } from './product-list/product-list.component';
import { SaperProfileComponent } from './saler-panel/saper-profile/saper-profile.component';
import { EditUserProfileComponent } from './edit-user-profile/edit-user-profile.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import {ChangePasswordComponent} from './change-password/change-password.component';
import { SubcategoryComponent } from './subcategory/subcategory.component';
import { SubcategoryProductsComponent } from './subcategory-products/subcategory-products.component';
import { AddressComponent } from './address/address.component';
import { PaymentComponent } from './user-panel/payment/payment.component';
import { MatRadioModule } from '@angular/material/radio';
import { ThankYouComponent } from './user-panel/thank-you/thank-you.component';
import { OrderHistoryComponent } from './user-panel/order-history/order-history.component';
import { RecentOrderComponent } from './saler-panel/recent-order/recent-order.component';
import { MatIconModule } from '@angular/material/icon';
import { ReactiveFormsModule } from '@angular/forms';
import { TransactionComponent } from './transaction/transaction.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { OtpChangePasswordComponent } from './otp-change-password/otp-change-password.component';
import { VerifyOtpComponent } from './verify-otp/verify-otp.component';
import { AdminSellerProductlistComponent } from './admin/admin-seller-productlist/admin-seller-productlist.component';
import { AdminSubcategoryProductlistComponent } from './admin/admin-subcategory-productlist/admin-subcategory-productlist.component';
import { AdminAllordersComponent } from './admin/admin-allorders/admin-allorders.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { SalerPurchaseComponent } from './saler-purchase/saler-purchase.component';
import { UserEditAddressComponent } from './user-panel/user-edit-address/user-edit-address.component';
import { CountdownModule } from 'ngx-countdown';
import { AdminUserListComponent } from './admin/admin-user-list/admin-user-list.component';
import { InteriorDesignComponent } from './interior-design/interior-design.component';

import { AdminUserorderListComponent } from './admin/admin-userorder-list/admin-userorder-list.component';

import { BookConsultantComponent } from './book-consultant/book-consultant.component';

import { InteriorProductComponent } from './admin/interior-product/interior-product.component';
import { AdminInteriorCategoriesComponent } from './admin/admin-interior-categories/admin-interior-categories.component';
import { AdminInteriorSubcategoriesComponent } from './admin/admin-interior-subcategories/admin-interior-subcategories.component';
import { InteriorSubproductsComponent } from './interior-subproducts/interior-subproducts.component';







@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainareaComponent,
    FooterComponent,
    Img1Component,
    ProductDetailsComponent,
    AboutComponent,
    CartComponent,
    UserHomeComponent,
    SalereHomeComponent,
    SalerLoginComponent,
    SalerNavbarComponent,
    SalerSidenavbarComponent,
    AdminhomeComponent,
    AdminCatgoriesComponent,
    AdminproductComponent,
    AdminSubcatgoryComponent,
    AdminSellerlistComponent,
    LoginComponent,
    UserRegistrationComponent,
    AllCatgoriesComponent,
    ContactUsComponent,
    Img2Component,
    OtpComponent,
    SellerRegComponent,
    AdminNavbarComponent,
    AdminSideNavbarComponent,
    AdminFooterComponent,
    AdminProductListComponent,
    PrivacyPolicyComponent,
    TermsConditinComponent,
    AdminVendorListComponent,
    AdminSaleComponent,
    AdminCancelOrderComponent,
    AdminReturnOrderComponent,
    AdminBannerComponent,
    SalerSaleComponent,
    SalerOrderCalcelComponent,
    SalerReturnComponent,
    
    SalerPurchaseReturnComponent,
    SalerProductAddComponent,
    SellerProductListComponent,
    ProductListComponent,
    SaperProfileComponent,
    EditUserProfileComponent,
    UserProfileComponent,
    ChangePasswordComponent,
    SubcategoryComponent,
    SubcategoryProductsComponent,
    AddressComponent,
    PaymentComponent,
    ThankYouComponent,
    OrderHistoryComponent,
    RecentOrderComponent,
    TransactionComponent,
    ForgotpasswordComponent,
    OtpChangePasswordComponent,
    VerifyOtpComponent,
    AdminSellerProductlistComponent,
    AdminSubcategoryProductlistComponent,
    AdminAllordersComponent,
    SalerPurchaseComponent,
    UserEditAddressComponent,
    AdminUserListComponent,
    InteriorDesignComponent,
    AdminUserorderListComponent,
    BookConsultantComponent,
    InteriorProductComponent,
    AdminInteriorCategoriesComponent,
    AdminInteriorSubcategoriesComponent,
    InteriorSubproductsComponent,
   
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgbModule,
    CommonModule,
    MatRadioModule,
    MatIconModule,
    AngularEditorModule ,
    BrowserModule, CountdownModule ,
    ReactiveFormsModule
  ],
  providers: [CookieService, Globals],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {}
