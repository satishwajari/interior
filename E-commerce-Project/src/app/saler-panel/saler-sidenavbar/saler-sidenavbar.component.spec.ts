import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalerSidenavbarComponent } from './saler-sidenavbar.component';

describe('SalerSidenavbarComponent', () => {
  let component: SalerSidenavbarComponent;
  let fixture: ComponentFixture<SalerSidenavbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalerSidenavbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalerSidenavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
