import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalerPurchaseReturnComponent } from './saler-purchase-return.component';

describe('SalerPurchaseReturnComponent', () => {
  let component: SalerPurchaseReturnComponent;
  let fixture: ComponentFixture<SalerPurchaseReturnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalerPurchaseReturnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalerPurchaseReturnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
