import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InteriorSubproductsComponent } from './interior-subproducts.component';

describe('InteriorSubproductsComponent', () => {
  let component: InteriorSubproductsComponent;
  let fixture: ComponentFixture<InteriorSubproductsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InteriorSubproductsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InteriorSubproductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
