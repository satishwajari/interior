import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalerLoginComponent } from './saler-login.component';

describe('SalerLoginComponent', () => {
  let component: SalerLoginComponent;
  let fixture: ComponentFixture<SalerLoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalerLoginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalerLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
