import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalerPurchaseComponent } from './saler-purchase.component';

describe('SalerPurchaseComponent', () => {
  let component: SalerPurchaseComponent;
  let fixture: ComponentFixture<SalerPurchaseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalerPurchaseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalerPurchaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
