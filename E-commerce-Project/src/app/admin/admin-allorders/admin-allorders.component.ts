import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataService } from 'src/app/services/data.service';
import { Globals } from 'src/app/services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { Urlcall } from 'src/app/services/urll';
import { address } from 'src/app/models/address'; 


@Component({
  selector: 'app-admin-allorders',
  templateUrl: './admin-allorders.component.html',
  styleUrls: ['./admin-allorders.component.scss']
})
export class AdminAllordersComponent implements OnInit {
  data: any;
  status: any;
  currentAddr = new address;
  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }


  ngOnInit(): void {
    
    this.status = 'pending';
    this.getdata(this.status);
    
  }


  getdata(status){
    this.status = status;
      this.spinner.show();
      this.dataService.get(Urlcall.fetchAllOrderAdmin+this.status).subscribe(res=>{
        this.spinner.hide();
        this.data = res['data'];
      },err=>{this.spinner.hide();})
      }

      address(addressItem){
        this.currentAddr = addressItem;
        console.log("Curent Addr : " + JSON.stringify(this.currentAddr));
      }
    


     
}
