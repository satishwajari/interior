export class product{
    _id:any;
    productName:any;
    price:any;
    image:any;
    summery:any;
    category_id:any;
    seller_id: any;
    subCategory_id: any
    product_discount_price:any
    product_price_parcentage:any
    product_selling_price:any
    item_height:any
    item_length:any
    item_breadth:any
    item_weight:any
    shipping_charge:any
    categoryName:any 
    createdAt:any 
    stock:any;
    admin_profit_parcentage:any;
    status:any;
    product_discount_parcentage:any;
    original_price:any;
}
