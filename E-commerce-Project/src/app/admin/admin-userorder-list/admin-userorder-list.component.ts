import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from '../../services/data.service';
import { Globals } from '../../services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Urlcall } from '../../services/urll';
import { consultant } from 'src/app/models/consultant';

@Component({
  selector: 'app-admin-userorder-list',
  templateUrl: './admin-userorder-list.component.html',
  styleUrls: ['./admin-userorder-list.component.scss']
})
export class AdminUserorderListComponent implements OnInit {
 data:any;
  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.getdata();
  }
  getdata(){
    this.spinner.show();
    this.dataService.get(Urlcall.fetchConsultant).subscribe(res=>{
      this.spinner.hide();
      this.data = res['data'];
    },err=>{this.spinner.hide();})
    }

}
