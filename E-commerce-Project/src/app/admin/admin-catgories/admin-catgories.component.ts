import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataService } from '../../services/data.service';
import { Globals } from '../../services/globalvariable';
import { product } from 'src/app/models/product'; 
import { ToastrService } from 'ngx-toastr';
import {
  NgbModal,
  ModalDismissReasons,
  NgbModalRef,
} from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse, HttpEventType } from '@angular/common/http';
import { Urlcall } from '../../services/urll';
import { category } from '../../models/category';
@Component({
  selector: 'app-admin-catgories',
  templateUrl: './admin-catgories.component.html',
  styleUrls: ['./admin-catgories.component.scss']
})
export class AdminCatgoriesComponent implements OnInit {
  categoryName: any;
  data: any[];
  product = new product;
  currentCategory = new category;
  

  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.getdata();
   
  }
  openModel(fileUpload){
    this.modalService.open(fileUpload, { centered: true });
  }

  percent: number;
  uploadSuccessuid: boolean;
  fileData: File = null;
  previewUrl:any ;
  fileUploadProgress: string = null;
  uploadedFilePath: string = null;

  
  fileProgress(fileInput: any) {
    this.fileData = <File>fileInput.target.files[0];
  }

uploadFile(){
  this.spinner.show();
  const formData = new FormData();
    formData.append('categoryName',this.categoryName );
    if(this.fileData){
      formData.append('files', this.fileData,this.fileData.name);
    }
this.dataService.postFile(Urlcall.cateogaryAdd,formData).subscribe(res=>{
  console.log(res);
  if (res.type === HttpEventType.UploadProgress) {
      this.percent = Math.round(100 * res.loaded / res.total);
  } else if (res instanceof HttpResponse) {
    this.uploadSuccessuid = true;
    this.toastr.success(res['message']);
    this.spinner.hide();
    this.getdata();
  }
},err=>{this.spinner.hide()})
}

getdata(){
  this.spinner.show();
  this.dataService.get(Urlcall.fetchAllCateogary).subscribe(res=>{
    this.spinner.hide();
    this.data = res['data'];
  },err=>{ this.spinner.hide();})
}

deleteproduct(id){
  this.spinner.show();
  this.dataService.delete(Urlcall.deletecategory+id).subscribe(res=>{
    this.spinner.hide();
    this.getdata();
    this.toastr.success(res['message'])
  },err=>{this.spinner.hide();
    this.toastr.warning(err['message'])
  })
}

updateCategory(){
  this.spinner.show();
  const formData = new FormData();
  formData.append('_id',this.currentCategory._id );
  formData.append('categoryName',this.currentCategory.categoryName );
    if(this.fileData){
      formData.append('files', this.fileData,this.fileData.name);
    }
this.dataService.putFile(Urlcall.updateCategory,formData).subscribe(res=>{
  console.log(res);
  if (res.type === HttpEventType.UploadProgress) {
      this.percent = Math.round(100 * res.loaded / res.total);
  } else if (res instanceof HttpResponse) {
    this.uploadSuccessuid = true;
    this.toastr.success(res['message']);
    this.spinner.hide();
    this.getdata();
  }
},err=>{this.spinner.hide()})
}
editcat(categorydata){
  this.currentCategory = categorydata;
}

 

addSub(id){
this.globals.CatId = id;
this.router.navigate(['/','Admin-Subcategory']);
}
}
