import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataService } from '../../services/data.service';
import { Globals } from '../../services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { Urlcall } from '../../services/urll';

export class cal{
  approved_orders: any = 0;
pending_orders: any = 0;
total_orders:any = 0;
total_products: any = 0;
}

@Component({
  selector: 'app-salere-home',
  templateUrl: './salere-home.component.html',
  styleUrls: ['./salere-home.component.scss']
})
export class SalereHomeComponent implements OnInit {
  data: any[];
  dashboad= new cal;
  constructor(public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }


  ngOnInit(): void {
    this.globals.id = this.cookieService.get('id');
    this.getdata();
    this.Recentorder();
  }
getdata(){
  this.spinner.show();
  this.dataService.get(Urlcall.sellerDashboad+this.globals.id).subscribe(res=>{
    this.spinner.hide();
    this.dashboad = res['data'];
  },err=>{this.spinner.hide();})
  
}
Recentorder(){
  this.spinner.show();
  this.dataService.get(Urlcall.fetchRecentOrderTOPTen+this.globals.id).subscribe(res=>{
    this.spinner.hide();
    this.data = res['data'];
  },err=>{this.spinner.hide();})
  }
  
}
