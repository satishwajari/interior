import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCancelOrderComponent } from './admin-cancel-order.component';

describe('AdminCancelOrderComponent', () => {
  let component: AdminCancelOrderComponent;
  let fixture: ComponentFixture<AdminCancelOrderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminCancelOrderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCancelOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
