import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TermsConditinComponent } from './terms-conditin.component';

describe('TermsConditinComponent', () => {
  let component: TermsConditinComponent;
  let fixture: ComponentFixture<TermsConditinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TermsConditinComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TermsConditinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
