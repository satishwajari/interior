import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';
import { Globals } from '../../services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Urlcall } from '../../services/urll';
import { updateLanguageServiceSourceFile } from 'typescript';
import { product } from 'src/app/models/product';

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.scss']
})
export class OrderHistoryComponent implements OnInit {
data:any;

  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }
  
    ngOnInit(): void {
      this.globals.id = this.cookieService.get('id');
      this.getData();
  }
getData(){
this.spinner.show();
this.dataService.get(Urlcall.orderHistory+this.globals.id).subscribe(res=>{
  this.spinner.hide();
  this.data = res['data'];
},err=>{this.spinner.hide();})
}
cancelOrder(id){
  console.log("Order id : " + id);
  this.spinner.show();
  let data ={
    _id: id
  }
  this.dataService.put(Urlcall.updateCancelOrder,data).subscribe(res=>{
   this.spinner.hide();
   //this.toastr.success(res['message']);
   this.getData();
   
 },err=>{ this.spinner.hide();this.toastr.warning(err['message'])})
}
// tracking(id){
//   this.globals.order_id = id;
//   this.cookieService.set('order_id',this.globals.order_id);
//   this.router.navigate(['/','tracking'])
// }

}
