import { Component, OnInit, Inject } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';
import { Globals } from '../../services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Urlcall } from '../../services/urll';
import { TransportCharge } from 'src/app/models/Transport-charge';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
  data;
  cartStatus:any;
  mode:any;
  calAmount = new TransportCharge;
  cod:any;
  

  constructor(@Inject(DOCUMENT) private document: Document,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }


  ngOnInit(): void {
    this.globals.address_id = this.cookieService.get('address_id');
    this.globals.id = this.cookieService.get('id');
    this.globals.BuyNowproduct_id = this.cookieService.get('BuyNowproduct_id');
    if(this.globals.address_id == ""){
      this.router.navigate(['/','Cart']);
    }
    if(this.globals.BuyNowproduct_id == ""){
      this.cartStatus = 'true';
      this.TransportCharge();
    }else{
      this.cartStatus = 'false';
     this.SingleProductCharge();
    }
  
   
   
  }


  TransportCharge(){ 
    {
    let data={
      
        user_id: this.globals.id,
        _id:  this.globals.address_id
      }
    
   this.spinner.show();
    this.dataService.post(Urlcall.transportCharge,data).subscribe(res=>{
      this.spinner.hide();
      this.calAmount = res['data'];
    
  },err=>{ this.spinner.hide();})
  }
}




  placeorder(){ 
    let data={
        user_id: this.globals.id,
        address_id: this.globals.address_id,
        paymentMode: this.mode ,
        product_id: this.globals.BuyNowproduct_id,
        cartStatus: this.cartStatus
        
   }
    this.spinner.show();
    this.dataService.post(Urlcall.transactionAdd,data).subscribe(res=>{
      this.spinner.hide();
       this.data = res['data'];
       if(res['status']){
         if(this.mode == 'Online'){
          this.onNavigate(this.data.url);
         }else{
          this.router.navigate(['/','Payment-Message'])
         }
       

      
       }
      },err=>{ this.spinner.hide();})
    }
    select(mode){
      this.mode = mode 
      
    }





    
    onNavigate(url){
      // your logic here.... like set the url 
      // const url = 'https://www.google.com';
      // window.open(url, '_blank');
      this.document.location.href = url;
  }



  SingleProductCharge(){ 
    {
    let data={
      user_id: this.globals.id,
      address_id: this.globals.address_id,
      _id: this.globals.BuyNowproduct_id
      }
     
   this.spinner.show();
    this.dataService.post(Urlcall.transCharge,data).subscribe(res=>{
      this.spinner.hide();
      let temp=res['data'];
    this.calAmount = temp[0];
    
  },err=>{ this.spinner.hide();})
  }
}





}
