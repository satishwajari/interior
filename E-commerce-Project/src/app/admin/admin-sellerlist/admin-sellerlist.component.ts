import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from '../../services/data.service';
import { Globals } from '../../services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Urlcall } from '../../services/urll';

@Component({
  selector: 'app-admin-sellerlist',
  templateUrl: './admin-sellerlist.component.html',
  styleUrls: ['./admin-sellerlist.component.scss']
})
export class AdminSellerlistComponent implements OnInit {
  data: any[];

  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }


  ngOnInit(): void {
    this.getdata()
  }
getdata(){
  this.spinner.show();
  this.dataService.get(Urlcall.fetchSeller).subscribe(res=>{
    this.spinner.hide();
    this.data = res['data'];
  },err=>{ this.spinner.hide();})
}
}
