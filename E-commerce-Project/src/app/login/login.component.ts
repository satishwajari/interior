import { Component, OnInit, Inject  } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';
import { NgxSpinnerService } from "ngx-spinner";
import { DataService } from '../services/data.service';
import { Router, NavigationEnd } from '@angular/router';
import { loginuser } from '../models/login';
import { Urlcall } from '../services/urll';
import { Globals } from '../services/globalvariable';
import { filter } from 'minimatch';
import { DOCUMENT } from '@angular/common';
import { Location } from '@angular/common';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user = new loginuser;
  constructor(public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService,
     @Inject(DOCUMENT) private _document: any,private location: Location) { }

    ngOnInit(): void {
      this.recordHistory();
    
    }
    next(url){
      this.router.navigate(['/',url])
    }
  
    runMyAPI(){
     this.spinner.show();
    //  this.getPreviousUrl()
       this.dataService.post(Urlcall.userLogin,this.user).subscribe(res=>{
         this.spinner.hide();
         this.toastr.success(res['message']);
         if(res['status']){
           console.log(res);
           let temp = res['data'];
          
           if(temp.otpVerificationStatus == "true"){
             console.log(temp.otpVerificationStatus,"login true")
            this.globals.id = temp._id;
            this.globals.role = temp.role;
            this.cookieService.set('id',this.globals.id);
            this.cookieService.set('role',this.globals.role);
           if(this.globals.role == "user"){
             this.goBack();
            // this.next('Home');
           }else if (this.globals.role == "admin"){
            this.next('Admin');
           }else if (this.globals.role == "seller"){
            this.next('SalerHome');
           }}else{
            console.log(temp.otpVerificationStatus,"login false")
             this.globals.temp_id = temp._id;
             this.cookieService.set('temp_id',this.globals.temp_id);
             this.next('otp')
           }
           
         }
       },err=>{ this.spinner.hide();
        this.toastr.success(err['message']);})
     }
  
     openTerms(){
      this.next('terms');
     }
  
     private history = [];
  
     public recordHistory(): void
     {
       this.router.events
         .pipe()
         .subscribe(({urlAfterRedirects}: NavigationEnd) => {
           this.history = [...this.history, urlAfterRedirects];
          //  console.log('history',this.history)
         });
     }
   
     public getHistory(): string[]
     {
       return this.history;
     }
   
     public getPreviousUrl()
     {
      //  return this.history[this.history.length - 2] || this._document.referrer;
      console.log('history',this.history || this._document.referrer)
       console.log('history',this.history[this.history.length - 2] || this._document.referrer)
     }
     goBack() {
      this.location.back();
    }  
  }
  