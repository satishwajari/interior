import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from '../../services/data.service';
import { Globals } from '../../services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Urlcall } from '../../services/urll';
import { reguser } from '../../models/user-reg';
import { bankdetail } from '../../models/bankdetail';

export class address{
  _id:any;
  city: any;
  state: any;
  address_line1: any;
  address_line2: any;
  pinCode: any;
}

@Component({
  selector: 'app-admin-vendor-list',
  templateUrl: './admin-vendor-list.component.html',
  styleUrls: ['./admin-vendor-list.component.scss']
})
export class AdminVendorListComponent implements OnInit {
  data: any[];
  status:any;
  user = new reguser;
  editProfile: boolean;
  sellerAddress = new address;
  userBankDetails = new bankdetail;
  readonlyDetails: boolean=true;
  bankdetail: boolean=false;
  
  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }


  ngOnInit(): void {
    this.globals.id = this.cookieService.get('id');
    this.status = 'pending';
    this.getdata(this.status)

  }

  updateuser(id,status){
    let data ={
      seller_id:  id,
      status: status
    }
    this.dataService.put(Urlcall.AdminSellerUpdate,data).subscribe(res=>{
      this.getdata(this.status);
      this.toastr.success(res['message'])
    },err=>{
      this.toastr.warning(err['message'])
    })
  }


getdata(status){
  this.status = status;
  console.log(status);
  this.spinner.show();
  this.dataService.get(Urlcall.fetchSeller+'&status='+status).subscribe(res=>{
    this.spinner.hide();
    this.data = res['data'];
  },err=>{ this.spinner.hide();})
}

Editprofile() {
  //  this.router.navigate(['/', 'Edit_Profile'])
  this.editProfile = true;
}

updateDetail(){
  this.spinner.show();
  this.dataService.put(Urlcall.userupdate,this.user).subscribe(res=>{
   this.spinner.hide();
   this.toastr.success(res['message']);
   if(res['status']){
     this.editProfile = false;
   }
 },err=>{ this.spinner.hide();this.toastr.warning(err['message'])})
}

saveAddress(){
  this.spinner.show();
  this.dataService.put(Urlcall.userupdate,this.sellerAddress).subscribe(res=>{
   this.spinner.hide();
   this.toastr.success(res['message']);
   if(res['status']){
     this.editProfile = false;
   }
 },err=>{ this.spinner.hide();this.toastr.warning(err['message'])})

}

OpenSeller(sellerId){
  this.globals.sellerid = sellerId; 
  this.cookieService.set('sellerid',this.globals.sellerid);
  this.router.navigate(['/','adminseller'])

}

getBank(id){
  this.spinner.show();
  this.userBankDetails.user_id =id;
  this.dataService.get(Urlcall.fetchBankDetail+id).subscribe(res=>{
    this.spinner.hide();
    this.userBankDetails = res['data'];
  },err=>{ this.spinner.hide();this.toastr.warning(err['message'])})
}
updateBank(){
  this.spinner.show();
 
  this.dataService.put(Urlcall.updateBankDetail,this.userBankDetails).subscribe(res=>{
   this.spinner.hide();
   this.userBankDetails = res['data'];
   this.toastr.success(res['message']);
   

   
 },err=>{ this.spinner.hide();this.toastr.warning(err['message'])})
}


addbank(){
  this.bankdetail=!this.bankdetail;
  
 
}
EditBankDetail(){
  this.readonlyDetails= false;

}

}
