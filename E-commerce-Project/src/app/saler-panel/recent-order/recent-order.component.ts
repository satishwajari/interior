import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from '../../services/data.service';
import { Globals } from '../../services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Urlcall } from 'src/app/services/urll';
import { product } from 'src/app/models/product'; 
import pdfMake from "pdfmake/build/pdfmake";  
import pdfFonts from "pdfmake/build/vfs_fonts";  
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import { address } from 'src/app/models/address'; 


@Component({
  selector: 'app-recent-order',
  templateUrl: './recent-order.component.html',
  styleUrls: ['./recent-order.component.scss']
})
export class RecentOrderComponent implements OnInit {
  data: any;
  invoice= {
    customerName:'deepak',
    address :'h-57,adrash nagar, dhurwa, ranchi, 834004',
    email : 'dappsIndia@gmail.com',
    contactNo: '7909080854',
    products:[]  
  };
  currentAddr = new address;
  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }


  ngOnInit(): void {
    this.globals.id = this.cookieService.get('id');
    this.getdata();
    
  }

  getdata(){
    this.spinner.show();
    this.dataService.get(Urlcall.fetchRecentOrder+this.globals.id).subscribe(res=>{
      this.spinner.hide();
      this.data = res['data'];
    },err=>{ this.spinner.hide();})
  }
 

  updateOrder(id,status){
    this.spinner.show();
    let data ={
      order_id: id,
      orderStatus: status
      
      }
    this.dataService.put(Urlcall.updateOrder,data).subscribe(res=>{
     this.spinner.hide();
     this.toastr.success(res['message']);
     
     
   },err=>{ this.spinner.hide();this.toastr.warning(err['message'])})
}




  address(addressItem){
    this.currentAddr = addressItem;
    console.log("Curent Addr : " + JSON.stringify(this.currentAddr));
  }

  generatePDF(action,data) {    
    this.invoice.customerName = data.address_id.fullName;
    this.invoice.email = '';
    this.invoice.contactNo = data.address_id.mobile;
    this.invoice.address = data.address_id.addressLine1 + ', ' + data.address_id.addressLine2 + ', '+ data.address_id.city 
                            +', '+ data.address_id.state + ', '+ data.address_id.pinCode  
        this.invoice.products[0]= data.product_id;
    let docDefinition = {      
      content: [  
        {  
          text: 'Craftysmile.com',  
          fontSize: 16,  
          alignment: 'center',  
          color: '#047886'  
        },  
        {  
          text: 'INVOICE',  
          fontSize: 20,  
          bold: true,  
          alignment: 'center',  
          decoration: 'underline',  
          color: 'skyblue'  
        } ,  {  
          text: 'Customer Details',  
          style: 'sectionHeader'  
      }  ,
      {  
        columns: [  
            [  
                {  
                    text: this.invoice.customerName,  
                    bold: true  
                },  
                { text: this.invoice.address },  
                { text: this.invoice.email },  
                { text: this.invoice.contactNo }  
            ],  
            [  
                {  
                    text: `Date: ${new Date().toLocaleString()}`,  
                    alignment: 'right'  
                },  
                {  
                    text: `Bill No : ${((Math.random() * 1000).toFixed(0))}`,  
                    alignment: 'right'  
                }  
            ]  
        ]  
    }, 
    {  
      text: 'Order Details',  
      style: 'sectionHeader'  
  },  
  {  
      table: {  
          headerRows: 1,  
          widths: ['*', 'auto', 'auto', 'auto'],  
          body: [  
              ['Product', 'Price', 'Quantity', 'Amount'],  
              ...this.invoice.products.map(p => ([p.productName, p.price, data.quantity, (p.price * p.qty).toFixed(2)])),  
              [{ text: 'Total Amount', colSpan: 3 }, {}, {}, this.invoice.products.reduce((sum, p) => sum + (data.quantity * p.price), 0).toFixed(2)]  
          ]  
      }  
  },
  {text:'.'},
  {text:'.'},
  {  
    columns: [  
        [{ qr: this.invoice.customerName, fit: '50' }],  
        [{ text: 'Signature', alignment: 'right', italics: true }],  
    ]  
},   {  
  ul: [  
    'Order can be return in max 10 days.',  
    'Warrenty of the product will be subject to the manufacturer terms and conditions.',  
    'This is system generated invoice.',  
  ],  
}     
  ],  
  styles: {  
      sectionHeader: {  
          bold: true,  
          decoration: 'underline',  
          fontSize: 14,  
          margin: [0, 15, 0, 15]  
      }  
  }
        
      };    
    
    if(action==='download'){    
      pdfMake.createPdf(docDefinition).download();    
    }else if(action === 'print'){    
      pdfMake.createPdf(docDefinition).print();          
    }else{    
      pdfMake.createPdf(docDefinition).open();          
    }    
    
  }

}
