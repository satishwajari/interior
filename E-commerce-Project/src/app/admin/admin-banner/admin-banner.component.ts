import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataService } from '../../services/data.service';
import { Globals } from '../../services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import {
  NgbModal,
  ModalDismissReasons,
  NgbModalRef,
} from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse, HttpEventType } from '@angular/common/http';
import { Urlcall } from 'src/app/services/urll';

@Component({
  selector: 'app-admin-banner',
  templateUrl: './admin-banner.component.html',
  styleUrls: ['./admin-banner.component.scss']
})
export class AdminBannerComponent implements OnInit {
  bannerNameData: any;
  bannerimageData: any;
  data: any;

  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }


  ngOnInit(): void {
    this.getdata();
  }
  openModel(fileUpload){
    this.modalService.open(fileUpload, { centered: true });
  }

  percent: number;
  uploadSuccessuid: boolean;
  fileData: File = null;
  previewUrl:any ;
  fileUploadProgress: string = null;
  uploadedFilePath: string = null;
  
  fileProgress(fileInput: any) {
    this.fileData = <File>fileInput.target.files[0];
  }

uploadFile(){
  const formData = new FormData();
    formData.append('bannerName',this.bannerNameData );
    if(this.fileData){
      formData.append('files', this.fileData,this.fileData.name);
    }
this.dataService.postFile(Urlcall.addbanner,formData).subscribe(res=>{
  console.log(res);
  if (res.type === HttpEventType.UploadProgress) {
      this.percent = Math.round(100 * res.loaded / res.total);
  } else if (res instanceof HttpResponse) {
    this.uploadSuccessuid = true;
    this.toastr.success(res['message']);
    this.getdata();
  }
})
}
  
  deletebanner(id){
    this.spinner.show();
    this.dataService.delete(Urlcall.deletebanner+id).subscribe(res=>{
      this.spinner.hide();
      this.getdata();
      this.toastr.success(res['message'])
    },err=>{this.spinner.hide();
      this.toastr.warning(err['message'])
    })
  }


  getdata(){
    this.spinner.show();
    this.dataService.get(Urlcall.fetchbannerData).subscribe(res=>{
      this.spinner.hide();
      this.data = res['data'];
      
    },err=>{ this.spinner.hide();})
  }
}
