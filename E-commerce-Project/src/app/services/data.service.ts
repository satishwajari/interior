import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class DataService {
  reject(arg0: string) {
    throw new Error('Method not implemented.');
  }

  constructor(public http: HttpClient) { }



  get(url: string): Observable<any> {
    return this.http.get(url);
  }

  post(url: string, body): Observable<any> {
    return this.http.post(url, body);
  }

  postFile(url: string, body): Observable<any> {
    return this.http.post(url, body, { reportProgress: true, observe: 'events' });
  }

  putFile(url: string, body): Observable<any> {
    return this.http.put(url, body, { reportProgress: true, observe: 'events' });
  }

  put(url: string, body: any): Observable<any> {
    return this.http.put(url, body);
  }

  delete(url: string): Observable<any> {
    return this.http.delete(url);
  }

  patch(url: string, body: any): Observable<any> {
    return this.http.patch(url, JSON.stringify(body));
  }
}
