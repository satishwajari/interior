import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { CountdownModule,CountdownComponent,CountdownConfig } from 'ngx-countdown';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataService } from '../services/data.service';
import { Globals } from '../services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { Urlcall } from '../services/urll';

@Component({
  selector: 'app-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.scss']
})
export class OtpComponent implements OnInit {
  data: any;
  otp: any;
  timmer=30;
  reSendButton:boolean=false;

  constructor(public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }
    ngOnInit(): void {
      this.globals.temp_id = this.cookieService.get('temp_id');
    }
  
  
    OtpVerify(){ 
      {
      let data={
        user_id:  this.globals.temp_id,
        otp: this.otp
        }
      
     this.spinner.show();
      this.dataService.post(Urlcall.OtpVerification,data).subscribe(res=>{
        this.spinner.hide();
        this.toastr.success(res['message'])
         this.data = res['data'];
         if (res['status']){
           let temp=this.data[0];
           this.globals.id=temp._id;
           this.cookieService.set('id',this.globals.id);
           this.globals.role=temp.role;
           this.cookieService.set('role',this.globals.role);
           if(this.globals.role=='user'){
            this.router.navigate(['/'])
           }else {
            this.router.navigate(['/','SalerHome'])
           }
          
           this.globals.otpstatus = true;
          
         }
      
        },err=>{ this.spinner.hide();
          this.toastr.success(err['message'])})
       }
  }
  handleEvent(event){
    console.log('event', event);
    if(event.action == 'done'){
      this.reSendButton = true;
    }
  }

  resendOtp(){ 
    let data={
      user_id: this.globals.temp_id,
    
   }
    this.spinner.show();
    this.dataService.post(Urlcall.reSendOtp,data).subscribe(res=>{
      this.spinner.hide();
      this.timmer=30;
       this.data = res['data'];
      },err=>{ this.spinner.hide();})
    }

  

 

  }
