import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtpChangePasswordComponent } from './otp-change-password.component';

describe('OtpChangePasswordComponent', () => {
  let component: OtpChangePasswordComponent;
  let fixture: ComponentFixture<OtpChangePasswordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OtpChangePasswordComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtpChangePasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
