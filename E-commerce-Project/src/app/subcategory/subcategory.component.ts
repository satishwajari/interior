import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataService } from 'src/app/services/data.service';
import { Globals } from 'src/app/services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { Urlcall } from '../services/urll';

@Component({
  selector: 'app-subcategory',
  templateUrl: './subcategory.component.html',
  styleUrls: ['./subcategory.component.scss']
})
export class SubcategoryComponent implements OnInit {
  data: any;

  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }


  ngOnInit(): void {
    this.globals.CatId = this.cookieService.get('CatId');
    this.getdata();
    
  }
  getdata(){
    this.spinner.show();
    this.dataService.get(Urlcall.fetchAllSubCat+this.globals.CatId).subscribe(res=>{
      this.spinner.hide();
      this.data = res['data'];
    },err=>{ this.spinner.hide();})
  }
product(id,interiorstatus){
  this.globals.subCatId = id; this.cookieService.set('subCatId',this.globals.subCatId);
  if(interiorstatus == "true"){
    this.router.navigate(['/','interior-subproducts'])
  }
  else{
    this.router.navigate(['/','subproduct'])
  }
  

}



}
