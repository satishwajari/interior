import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSellerlistComponent } from './admin-sellerlist.component';

describe('AdminSellerlistComponent', () => {
  let component: AdminSellerlistComponent;
  let fixture: ComponentFixture<AdminSellerlistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminSellerlistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSellerlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
