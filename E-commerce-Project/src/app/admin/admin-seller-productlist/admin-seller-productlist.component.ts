import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataService } from 'src/app/services/data.service';
import { Globals } from 'src/app/services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { Urlcall } from 'src/app/services/urll'; 
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { product } from 'src/app/models/product'; 
import {NgbModal, ModalDismissReasons,NgbModalModule} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-admin-seller-productlist',
  templateUrl: './admin-seller-productlist.component.html',
  styleUrls: ['./admin-seller-productlist.component.scss']
})
export class AdminSellerProductlistComponent implements OnInit {

  NgbdModalBasic 
  closeResult = '';
  datamodel: NgbModalModule; 
  product = new product;
  data: any[];
status:any;
options: any;
  subcategorydata: any;
  currentImage: any;

  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }


  ngOnInit(): void {
    this.globals.sellerid = this.cookieService.get('sellerid');
 this.status = 'pending';
    this.getdata(this.status);
    this.category();

    

  }

  rejectproduct(id,status){
   let data ={
      _id: id,
      status: status
    }
    this.dataService.put(Urlcall.productupdate,data).subscribe(res=>{
      this.getdata('pending');
      this.toastr.success(res['message'])
    },err=>{
      this.toastr.warning(err['message'])
    })
  }


getdata(status){
  this.status = status;
this.spinner.show();
this.dataService.get(Urlcall.fetchproductstatus+"?status="+status+"&seller_id="+ this.globals.sellerid).subscribe(res=>{
  this.spinner.hide();
  this.data = res['data'];
},err=>{this.spinner.hide();})
}
edit(rel){
  this.product =rel;
  this.currentImage = this.product.image[0].img_name;
  console.log(this.product);
}


category(){
  this.spinner.show();
  this.dataService.get(Urlcall.fetchAllCateogary).subscribe(res=>{
    this.spinner.hide();
    this.options = res['data'];
  },err=>{ this.spinner.hide();})
}

subcategory(){
  this.spinner.show();
  this.dataService.get(Urlcall.fetchAllSubCat+this.product.category_id).subscribe(res=>{
    this.spinner.hide();
    this.subcategorydata = res['data'];
  },err=>{ this.spinner.hide();})
}



update(){
  this.spinner.show();
  this.dataService.put(Urlcall.updateProduct,this.product).subscribe(res=>{
   this.spinner.hide();
   this.modalService.dismissAll();
   this.toastr.success(res['message']);
   this.getdata('pending')

   
 },err=>{ this.spinner.hide();this.toastr.warning(err['message'])})
}


percent: number;
uploadSuccessuid: boolean;
fileData: File = null;
previewUrl:any ;
fileUploadProgress: string = null;
uploadedFilePath: string = null;

fileProgress(fileInput: any) {
  this.fileData = <File>fileInput.target.files[0];
  this.addImage()
}

addImage(){
this.spinner.show();
const formData = new FormData();
formData.append('_id',this.product._id);    

if(this.fileData){
  formData.append('files', this.fileData,this.fileData.name);
}
this.dataService.putFile(Urlcall.updateIMAGE,formData).subscribe(res=>{
console.log(res);
if (res.type === HttpEventType.UploadProgress) {
  this.percent = Math.round(100 * res.loaded / res.total);
} else if (res instanceof HttpResponse) {
this.uploadSuccessuid = true;
this.toastr.success(res['message']);
this.spinner.hide();
 this.getdata('pending');
//this.router.navigate(['/','seller-product-list'])
}
},err=>{this.toastr.warning(err['message']); this.spinner.hide();})
}
deleteImage(id){
this.spinner.show();
this.dataService.delete(Urlcall.deleteImage+id+'&product_id='+this.product._id).subscribe(res=>{
this.spinner.hide();
this.getdata('pending');
this.toastr.success(res['message'])
},err=>{this.spinner.hide();
this.toastr.warning(err['message'])
})
} 
selectImg(img){
  this.currentImage = img;
}   

open(content) {
  this.datamodel = this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
     this.closeResult = `Closed with: ${result}`;
   }, (reason) => {
     this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
   });
 }

 private getDismissReason(reason: any): string {
   if (reason === ModalDismissReasons.ESC) {
     return 'by pressing ESC';
   } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
     return 'by clicking on a backdrop';
   } else {
     return `with: ${reason}`;
   }
 }

}


