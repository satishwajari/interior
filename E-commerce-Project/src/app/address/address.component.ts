import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { DataService } from '../services/data.service';
import { Globals } from '../services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Urlcall } from '../services/urll';
import { address } from '../models/address';
import { TransportCharge } from '../models/Transport-charge';
import { identity } from 'rxjs';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})
export class AddressComponent implements OnInit {
  data: any[];
  Address = new address;
  addAddress = false;
  prodCost: any;
  calAmount = new TransportCharge;
  EditAddress = new address;
  
  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.globals.id = this.cookieService.get('id');
    this.globals.BuyNowproduct_id = this.cookieService.get('BuyNowproduct_id');
    this.getdata();
    this.getProductCost();
   
  }
  add(){
    this.Address.user_id = this.globals.id;
    this.spinner.show();
    this.dataService.post(Urlcall.AddDeliveryAddress,this.Address).subscribe(res=>{
      this.toastr.success(res['message']);
      this.spinner.hide();
      if(res['status']){
        this.Address = new address;
        this.addAddress = false;
        this.getdata();
      }
    },err=>{
      this.toastr.warning(err['message']);
      this.spinner.hide();
    })
  }

  getdata(){
    this.spinner.show();
    this.dataService.get(Urlcall.FetchdeliveryAddress+this.globals.id).subscribe(res=>{
      this.data = res['data'];
      this.spinner.hide();
      this.addressSelect(this.data[0]._id)
    },err=>{this.spinner.hide();})
  }

  placeorder(){ 
    if(this.globals.address_id){
      this.router.navigate(['/','User-Payment'])
    }else{
      this.toastr.warning("please select address first")
    }
  
    }

    addressSelect(id){
      this.globals.address_id = id;
      this.cookieService.set('address_id',this.globals.address_id);
      this.getProductCost();
      this.SingleProductCharge();
    }

    getProductCost(){
      this.spinner.show();
      this.dataService.get(Urlcall.productCost+this.globals.id+"&_id="+this.globals.BuyNowproduct_id+"&address_id="+this.globals.address_id).subscribe(res=>{
        this.spinner.hide();
        this.prodCost = res['data'];
      },err=>{this.spinner.hide();})
    }



   /* this api is used for total product cost*/


  //   TransportCharge(){ 
  //     {
  //     let data={
        
  //         user_id: this.globals.id,
  //         _id:  this.globals.address_id
  //       }
      
  //    this.spinner.show();
  //     this.dataService.post(Urlcall.transportCharge,data).subscribe(res=>{
  //       this.spinner.hide();
  //       this.calAmount = res['data'];
      
  //   },err=>{ this.spinner.hide();})
  //   }
  // }



  RemoveAddress(id){
    this.spinner.show();
    this.dataService.delete(Urlcall.deleteAddress+id).subscribe(res=>{
      this.spinner.hide();
      this.getdata();
      this.toastr.success(res['message'])
    },err=>{this.spinner.hide();
      this.toastr.warning(err['message'])
    })
  }


  updateAddress(){
    this.spinner.show()
  this.dataService.put(Urlcall.updateAddress,this.EditAddress).subscribe(res=>{
   this.spinner.hide();
   this.getdata();
   this.toastr.success(res['message']);
   
   
 },err=>{ this.spinner.hide();this.toastr.warning(err['message'])})

}

edit(rel){
  this.EditAddress =rel;
  
  
  console.log(this.EditAddress);
} 

SingleProductCharge(){ 
  {
  let data={
    user_id: this.globals.id,
    address_id: this.globals.address_id,
    _id: this.globals.BuyNowproduct_id
    }
   
 this.spinner.show();
  this.dataService.post(Urlcall.transCharge,data).subscribe(res=>{
    this.spinner.hide();
    let temp=res['data'];
    this.calAmount = temp[0];
  
},err=>{ this.spinner.hide();})
}
}

}
