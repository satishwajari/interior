import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataService } from 'src/app/services/data.service';
import { Globals } from 'src/app/services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { Urlcall } from '../services/urll';
import { interiorproduct } from '../models/interior-product';

@Component({
  selector: 'app-interior-subproducts',
  templateUrl: './interior-subproducts.component.html',
  styleUrls: ['./interior-subproducts.component.scss']
})
export class InteriorSubproductsComponent implements OnInit {
  data:any;
  interiorproduct = new interiorproduct;

  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.globals.subCatId = this.cookieService.get('subCatId');
    this.globals.CatId = this.cookieService.get('CatId');
    this.getdata();
    
    
  }
   getdata(){
     this.spinner.show();
     this.dataService.get(Urlcall.fetchInteriorProductBySubcategory+this.globals.subCatId).subscribe(res=>{
       this.spinner.hide();
       this.data = res['data'];
     },err=>{ this.spinner.hide();})
   }
   productview(id){
    this.globals.product_id =id; 
    this.cookieService.set('product_id',this.globals.product_id);
    console.log('productid',id);
    this.router.navigate(['/book-consultant'] ,{ queryParams: { id : id}});   
  }

 
//  Interiorcategory(){
//    this.spinner.show();
//    this.dataService.get(Urlcall.fetchInteriorProductBySubcategory+this.globals.subCatId).subscribe(res=>{
//      this.spinner.hide();
//      this.data= res['data'];
//    },err=>{ this.spinner.hide();})

//  }
}
