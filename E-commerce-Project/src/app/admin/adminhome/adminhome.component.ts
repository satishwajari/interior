import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from '../../services/data.service';
import { Globals } from '../../services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Urlcall } from 'src/app/services/urll';

export class cal{
  total_orders: any ;
  total_products: any ;
  total_profit: any ;
  total_sale: any ;
  total_revenue: any;
  total_stock: any;
  total_views:any; 
}


@Component({
  selector: 'app-adminhome',
  templateUrl: './adminhome.component.html',
  styleUrls: ['./adminhome.component.scss']
})
export class AdminhomeComponent implements OnInit {
  data: any[];
  dashboad= new cal;
  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }



  ngOnInit(): void {
    this.globals.id = this.cookieService.get('id');
    this.getdata();
    this.Dahboard();
    

  }
  getdata(){
    this.spinner.show();
    this.dataService.get(Urlcall.fetchRecentOrderAdmin).subscribe(res=>{
      this.spinner.hide();
      this.data = res['data'];
    },err=>{this.spinner.hide();})
    }

    Dahboard(){
      this.spinner.show();
      this.dataService.get(Urlcall.adminDash).subscribe(res=>{
        this.spinner.hide();
        this.dashboad = res['data'];
      },err=>{this.spinner.hide();})
      }

      recentProductView(){
        this.router.navigate(['/','AdminReturn-Order']);
      }

}
