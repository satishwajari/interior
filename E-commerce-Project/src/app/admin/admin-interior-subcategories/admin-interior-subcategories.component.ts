import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataService } from '../../services/data.service';
import { Globals } from '../../services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { HttpResponse, HttpEventType } from '@angular/common/http';
import {
  NgbModal,
  ModalDismissReasons,
  NgbModalRef,
} from '@ng-bootstrap/ng-bootstrap';
import { interiorsubcategory } from 'src/app/models/interior-subcategory';
import { interiorproduct } from 'src/app/models/interior-product';
import { Urlcall } from '../../services/urll';


@Component({
  selector: 'app-admin-interior-subcategories',
  templateUrl: './admin-interior-subcategories.component.html',
  styleUrls: ['./admin-interior-subcategories.component.scss']
})
export class AdminInteriorSubcategoriesComponent implements OnInit {
  data:any[];
  options: any[];
  subcategoryName:any;
  interiorsubcategory = new interiorsubcategory;
  interiorproduct = new interiorproduct;

  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }

  ngOnInit(): void {
    if(this.globals.CatId == null){
      this.router.navigate(['/','admin-interior-category']);
          }
    this.getdata();
  }
  openModel(fileUpload){
    this.modalService.open(fileUpload, { centered: true });
  }
  fileData: File = null;
  percent: number;
  uploadSuccessuid: boolean;
  uploadedFilePath: string = null;

  fileProgress(fileInput: any) {
    this.fileData = <File>fileInput.target.files[0];
  }

  uploadFile(){
    this.spinner.show();
    const formData = new FormData();
      formData.append('name',this.subcategoryName );
      formData.append('category_id',this.globals.CatId);
      if(this.fileData){
        formData.append('files', this.fileData,this.fileData.name);
      }
  this.dataService.postFile(Urlcall.interiorSubcategory,formData).subscribe(res=>{
    console.log(res);
    if (res.type === HttpEventType.UploadProgress) {
        this.percent = Math.round(100 * res.loaded / res.total);
    } else if (res instanceof HttpResponse) {
      this.uploadSuccessuid = true;
      this.toastr.success(res['message']);
      this.spinner.hide();
       this.getdata();
    }
  },err=>{this.spinner.hide()})
  }

  getdata(){
    this.spinner.show();
    this.dataService.get(Urlcall.fetchInteriorsubcategory+this.globals.CatId).subscribe(res=>{
      this.spinner.hide();
      this.data = res['data'];
    },err=>{ this.spinner.hide();})
  }

  deleteproduct(id){
    this.dataService.delete(Urlcall.deletesubcategory+id).subscribe(res=>{
      this.getdata();
      this.toastr.success(res['message'])
    },err=>{
      this.toastr.warning(err['message'])
    })
  }

  updateSubcategory(){
    this.spinner.show();
    const formData = new FormData();
    formData.append('_id',this.interiorsubcategory._id );
    formData.append('name',this.interiorsubcategory.name );
      if(this.fileData){
        formData.append('files', this.fileData,this.fileData.name);
      }
  this.dataService.putFile(Urlcall.updateSubcategory,formData).subscribe(res=>{
    console.log(res);
    if (res.type === HttpEventType.UploadProgress) {
        this.percent = Math.round(100 * res.loaded / res.total);
    } else if (res instanceof HttpResponse) {
      this.uploadSuccessuid = true;
      this.toastr.success(res['message']);
      this.spinner.hide();
      this.getdata();
    }
  },err=>{this.spinner.hide()})
  }
  editsubcat(subcategorydata){
    this.interiorsubcategory = subcategorydata;
  }

  opensubctegory(id){
    this.globals.subCatId = id;
    this.cookieService.set('subCatId',this.globals.subCatId);
    this.router.navigate(['/','interior-product']);
    
  }
}


