import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaperProfileComponent } from './saper-profile.component';

describe('SaperProfileComponent', () => {
  let component: SaperProfileComponent;
  let fixture: ComponentFixture<SaperProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaperProfileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaperProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
