import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from '../services/data.service'; 
import { Globals } from '../services/globalvariable'; 
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Urlcall } from '../services/urll';


@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  data: any;
  productName:any;
  

  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }



  ngOnInit(): void {
    this.productName =this.globals.search ;
    this.getdata();
  }
  
  getdata(){
    this.spinner.show();
    this.dataService.get(Urlcall.fetchproductName+this.globals.search).subscribe(res=>{
      this.spinner.hide();
      this.data = res['data'];
      
    },err=>{ this.spinner.hide();})
  }
   addcart(id){ 
     let data={
      user_id: this.globals.id,
      product_id: id
    }
     this.spinner.show();
     this.dataService.post(Urlcall.cartAdd,data).subscribe(res=>{
       this.spinner.hide();
        this.data = res['data'];
       },err=>{ this.spinner.hide();})
     }

     productview(id){
      this.globals.product_id =id;
      this.cookieService.set('product_id',this.globals.product_id);
      console.log('productid',id);
      this.router.navigate(['/Product-View'] ,{ queryParams: { id : id}});   
    }

}
