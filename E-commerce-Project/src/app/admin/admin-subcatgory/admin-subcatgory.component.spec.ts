import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSubcatgoryComponent } from './admin-subcatgory.component';

describe('AdminSubcatgoryComponent', () => {
  let component: AdminSubcatgoryComponent;
  let fixture: ComponentFixture<AdminSubcatgoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminSubcatgoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSubcatgoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
