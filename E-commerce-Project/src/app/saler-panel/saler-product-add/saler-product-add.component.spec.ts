import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalerProductAddComponent } from './saler-product-add.component';

describe('SalerProductAddComponent', () => {
  let component: SalerProductAddComponent;
  let fixture: ComponentFixture<SalerProductAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalerProductAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalerProductAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
