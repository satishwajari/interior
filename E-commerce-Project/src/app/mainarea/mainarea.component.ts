import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from '../services/data.service'; 
import { Globals } from '../services/globalvariable'; 
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Urlcall } from '../services/urll';

@Component({
  selector: 'app-mainarea',
  templateUrl: './mainarea.component.html',
  styleUrls: ['./mainarea.component.scss']
})
export class MainareaComponent implements OnInit {
  data: any;
  bannerData: any;
  categoryData: any;

  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }


  ngOnInit(): void {
    this.globals.id =this.cookieService.get('id')
    
    this.getdata();
  }
 
  getdata(){
    this.spinner.show();
    this.dataService.get(Urlcall.fetchdashboardData).subscribe(res=>{
      this.spinner.hide();
      this.data = res['data'];
      this.bannerData = this.data['banner']
      this.categoryData = this.data['category']
    },err=>{ this.spinner.hide();})
  }
  
    
  addcart(id){ 
    if(this.globals.id){
    let data={
      user_id: this.globals.id,
      product_id: id
    }
   this.spinner.show();
    this.dataService.post(Urlcall.cartAdd,data).subscribe(res=>{
      this.spinner.hide();
       this.data = res['data'];
       this.router.navigate(['/','Cart'])
  },err=>{ this.spinner.hide();})
  }else{
    this.router.navigate(['/signin'])
  }
}
  
  
       

  categorypage(id){
    this.globals.CatId = id;
    this.router.navigate(['/','all-category']);
    }
    productview(id){
      this.globals.product_id =id;
      this.cookieService.set('product_id',this.globals.product_id);
      console.log('productid',id);
      this.router.navigate(['/Product-View'] ,{ queryParams: { id : id}});   
    }
    category(id){
      this.globals.CatId = id;
      this.cookieService.set('CatId',this.globals.CatId)
      this.router.navigate(['/','subcategory'])
    }

    Productcategory(id){
      this.cookieService.delete('subCatId');
      this.globals.CatId = id;
      this.cookieService.set('CatId',this.globals.CatId)
      this.router.navigate(['/','subproduct'])
    }

  }
 