import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';
import { Globals } from '../../services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { Urlcall } from '../../services/urll';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
import { product } from 'src/app/models/product'; 
import { subcategory } from '../../models/subcategory';

@Component({
  selector: 'app-admin-subcatgory',
  templateUrl: './admin-subcatgory.component.html',
  styleUrls: ['./admin-subcatgory.component.scss']
})
export class AdminSubcatgoryComponent implements OnInit {
  subcategoryName: any;
  data: any[];
  options: any[];
  product = new product;
  currentSubcategory = new subcategory;
  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }

  ngOnInit(): void {
    if(this.globals.CatId == null){
this.router.navigate(['/','Admin-Category']);
    }
    this.getdata();

    
  }
  openModel(fileUpload){
    this.modalService.open(fileUpload, { centered: true });
  }

  percent: number;
  uploadSuccessuid: boolean;
  fileData: File = null;
  previewUrl:any ;
  fileUploadProgress: string = null;
  uploadedFilePath: string = null;
  
  fileProgress(fileInput: any) {
    this.fileData = <File>fileInput.target.files[0];
  }

uploadFile(){
  this.spinner.show();
  const formData = new FormData();
    formData.append('name',this.subcategoryName );
    formData.append('category_id',this.globals.CatId);
    if(this.fileData){
      formData.append('files', this.fileData,this.fileData.name);
    }
this.dataService.postFile(Urlcall.subCatAdd,formData).subscribe(res=>{
  console.log(res);
  if (res.type === HttpEventType.UploadProgress) {
      this.percent = Math.round(100 * res.loaded / res.total);
  } else if (res instanceof HttpResponse) {
    this.uploadSuccessuid = true;
    this.toastr.success(res['message']);
    this.getdata();
    this.spinner.hide();
  }
},err=>{this.spinner.hide()})
}

getdata(){
  this.spinner.show();
  this.dataService.get(Urlcall.fetchAllSubCat+this.globals.CatId).subscribe(res=>{
    this.spinner.hide();
    this.data = res['data'];
  },err=>{ this.spinner.hide();})
}
deleteproduct(id){
  this.dataService.delete(Urlcall.deletesubcategory+id).subscribe(res=>{
    this.getdata();
    this.toastr.success(res['message'])
  },err=>{
    this.toastr.warning(err['message'])
  })
}

// updateSubcategory(){
//   this.spinner.show();
//   this.dataService.put(Urlcall.updateCategory,this.product).subscribe(res=>{
//    this.spinner.hide();
//    this.getdata();
//    this.toastr.success(res['message']);
   
   
//  },err=>{ this.spinner.hide();this.toastr.warning(err['message'])})
// }


updateSubcategory(){
  this.spinner.show();
  const formData = new FormData();
  formData.append('_id',this.currentSubcategory._id );
  formData.append('name',this.currentSubcategory.name );
    if(this.fileData){
      formData.append('files', this.fileData,this.fileData.name);
    }
this.dataService.putFile(Urlcall.updateSubcategory,formData).subscribe(res=>{
  console.log(res);
  if (res.type === HttpEventType.UploadProgress) {
      this.percent = Math.round(100 * res.loaded / res.total);
  } else if (res instanceof HttpResponse) {
    this.uploadSuccessuid = true;
    this.toastr.success(res['message']);
    this.spinner.hide();
    this.getdata();
  }
},err=>{this.spinner.hide()})
}
editsubcat(subcategorydata){
  this.currentSubcategory = subcategorydata;
}








addProdict(id){
this.globals.subCatId = id;
this.router.navigate(['/','Admin-Subcategory']);
}

opensubctegory(id){
  this.globals.subCatId = id;
  this.cookieService.set('subCatId',this.globals.subCatId);
  this.router.navigate(['/','subcategoryproductlist']);
  
}





}
