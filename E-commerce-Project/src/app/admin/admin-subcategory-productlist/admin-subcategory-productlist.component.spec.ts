import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSubcategoryProductlistComponent } from './admin-subcategory-productlist.component';

describe('AdminSubcategoryProductlistComponent', () => {
  let component: AdminSubcategoryProductlistComponent;
  let fixture: ComponentFixture<AdminSubcategoryProductlistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminSubcategoryProductlistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSubcategoryProductlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
