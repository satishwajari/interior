import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataService } from '../services/data.service';
import { Globals } from '../services/globalvariable'; 
import { ToastrService } from 'ngx-toastr';
import {
  NgbModal,
  ModalDismissReasons,
  NgbModalRef,
} from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse, HttpEventType } from '@angular/common/http';
import { Urlcall } from '../services/urll'; 


@Component({
  selector: 'app-all-catgories',
  templateUrl: './all-catgories.component.html',
  styleUrls: ['./all-catgories.component.scss']
})
export class AllCatgoriesComponent implements OnInit {
  data: any;

  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }



  ngOnInit(): void {
     this.getdata();
    
  }
    getdata(){
      this.spinner.show();
     this.dataService.get(Urlcall.fetchAllCateogary).subscribe(res=>{
       this.spinner.hide();
       this.data = res['data'];
     },err=>{ this.spinner.hide();})
   }
   category(id){
     this.globals.CatId = id;
     this.cookieService.set('CatId',this.globals.CatId)
     this.router.navigate(['/','subcategory'])
   }

} 
