import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataService } from '../services/data.service';
import { Globals } from '../services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { Urlcall } from '../services/urll';


@Component({
  selector: 'app-verify-otp',
  templateUrl: './verify-otp.component.html',
  styleUrls: ['./verify-otp.component.scss']
})
export class VerifyOtpComponent implements OnInit {
  data: any;
  otp: any;

  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }


    ngOnInit(): void {
      this.globals.user_id = this.cookieService.get('user_id');
     
    }
  
  
    OtpVerify(){ 
      {
      let data={
        
        _id:  this.globals.user_id,
        
        otp: this.otp
        
      
        }
      
     this.spinner.show();
      this.dataService.post(Urlcall.OtpVerification,data).subscribe(res=>{
        this.spinner.hide();
        this.toastr.success(res['message'])
         this.data = res['data'];
         if (res['status']){
           this.router.navigate(['/','otppassword'])
           this.globals.otpstatus = true;
          
         }
      
        },err=>{ this.spinner.hide();
          this.toastr.success(err['message'])})
       }
  }
  }