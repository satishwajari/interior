import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataService } from '../services/data.service';
import { Globals } from '../services/globalvariable';
import { ToastrService } from 'ngx-toastr';
import { Urlcall } from '../services/urll';


@Component({
  selector: 'app-otp-change-password',
  templateUrl: './otp-change-password.component.html',
  styleUrls: ['./otp-change-password.component.scss']
})
export class OtpChangePasswordComponent implements OnInit {

  data: any;
 password: any;
 cpassword:any;
  constructor(private modalService: NgbModal,public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }


  ngOnInit(): void {
if (!this.globals.otpstatus ){
  this.router.navigate(['/','forgotpassword'])
}
    this.globals.user_id = this.cookieService.get('user_id');
  }

  ChangePassword(){
    this.spinner.show();
    let data ={
      
        user_id: this.globals.user_id,

        newPassword: this.password
      
      }
    this.dataService.put(Urlcall.changepassword,data).subscribe(res=>{
     this.spinner.hide();
     this.toastr.success(res['message']);
     if (res['status']){
       this.router.navigate(['/','signin']);
     }
     
     
   },err=>{ this.spinner.hide();this.toastr.warning(err['message'])})
}

 submit(){
   if (this.password == this.cpassword){
     this.ChangePassword();

   }else {
     this.toastr.warning('wrong conform password');
   }
 }


}
