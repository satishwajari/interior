import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalerOrderCalcelComponent } from './saler-order-calcel.component';

describe('SalerOrderCalcelComponent', () => {
  let component: SalerOrderCalcelComponent;
  let fixture: ComponentFixture<SalerOrderCalcelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalerOrderCalcelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalerOrderCalcelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
