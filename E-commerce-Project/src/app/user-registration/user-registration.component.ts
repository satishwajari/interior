import { Component, OnInit } from '@angular/core';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';
import { NgxSpinnerModule, NgxSpinnerService } from "ngx-spinner";
import { DataService } from '../services/data.service';
import { Router } from '@angular/router';
import { reguser } from '../models/user-reg';
import { Urlcall } from '../services/urll';
import { Globals } from '../services/globalvariable';

@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.scss']
})
export class UserRegistrationComponent implements OnInit {

  user = new reguser;

  constructor(public cookieService: CookieService, private router: Router,private spinner: NgxSpinnerService,
    public dataService: DataService, public globals: Globals, private toastr: ToastrService) { }

    ngOnInit(): void {
  
    }
    next(){
      this.router.navigate(['/','otp'])
    }
  
    runMyAPI(){ 
      this.spinner.show();
       this.dataService.post(Urlcall.userReg,this.user).subscribe(res=>{
         this.spinner.hide();
         this.toastr.success(res['message'])
         if(res['status']){
           console.log(res);
           this.globals.temp_id = res['data']._id;
          //  this.globals.role = res['data'].role;
           this.cookieService.set('temp_id',this.globals.temp_id);
          //  this.cookieService.set('role',this.globals.role);
           this.next();
         }
       },err=>{ this.spinner.hide();
        this.toastr.success(err['message'])})
     }
  
     otp(){
       this.spinner.show();
       
     }
  }